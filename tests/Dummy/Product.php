<?php 
namespace Cetria\Laravel\Api\Tests\Dummy;

use Cetria\Laravel\Form\Traits\UseCastToForm;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Cetria\Laravel\Filter\Traits\ExtensionForModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Cetria\Laravel\Form\Interfaces\CastToFormInterface;
use Cetria\Laravel\Helpers\Test\Dummy\Product as DummyProduct;

class Product extends DummyProduct implements CastToFormInterface
{
    use UseCastToForm;
    use ExtensionForModel;

    public function basketItemsHasMany(): HasMany
    {
        return $this->hasMany(Basket::class, 'product_id');
    }

    public function basketItemHasOne(): HasOne
    {
        return $this->hasOne(Basket::class, 'product_id');
    }
}