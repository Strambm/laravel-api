<?php 
namespace Cetria\Laravel\Api\Tests\Dummy;

use Cetria\Laravel\Form\Traits\UseCastToForm;
use Cetria\Laravel\Form\Interfaces\CastToFormInterface;
use Cetria\Laravel\Helpers\Test\Dummy\User as DummyUser;

class User extends DummyUser implements CastToFormInterface
{
    use UseCastToForm;
}