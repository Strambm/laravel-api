<?php 
namespace Cetria\Laravel\Api\Tests\Dummy;

use Cetria\Laravel\Helpers\Test\Dummy\Category;
use Cetria\Laravel\Helpers\Test\Dummy\Product as DummyProduct;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ProductForUpdatePivot extends DummyProduct
{
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'category_product', 'category_id', 'product_id')->withPivot(['tmp']);
    }
}