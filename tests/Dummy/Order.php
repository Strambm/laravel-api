<?php 
namespace Cetria\Laravel\Api\Tests\Dummy;

use Cetria\Laravel\Form\Traits\UseCastToForm;
use Cetria\Laravel\Form\Interfaces\CastToFormInterface;
use Cetria\Laravel\Helpers\Test\Dummy\Order as DummyOrder;

class Order extends DummyOrder implements CastToFormInterface
{
    use UseCastToForm;

    protected $table = 'orders';

    protected $casts = [
        'date' =>'datetime:Y-m-d H:i:s'
    ];
}