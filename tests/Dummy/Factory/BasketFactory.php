<?php

namespace Cetria\Laravel\Api\Tests\Dummy\Factory;

use Illuminate\Database\Eloquent\Model;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Api\Interfaces\CrudModelFactoryInterface;
use Cetria\Laravel\Helpers\Test\Dummy\Factory\BasketFactory as DummyBasketFactory ;

class BasketFactory extends DummyBasketFactory
implements CrudModelFactoryInterface
{
    public function forModel(Model $model): static
    {
        if($model instanceof Product) {
            return $this->state([
                'product_id' => $model->getKey(),
            ]);
        }
        return $this;
    }
}
