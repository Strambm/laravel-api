<?php 
namespace Cetria\Laravel\Api\Tests\Dummy;

use Cetria\Laravel\Form\Traits\UseCastToForm;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Cetria\Laravel\Form\Interfaces\CastToFormInterface;
use Cetria\Laravel\Api\Tests\Dummy\Factory\BasketFactory;
use Cetria\Laravel\Helpers\Test\Dummy\Basket as DummyBasket;

class Basket extends DummyBasket implements CastToFormInterface
{
    use UseCastToForm;

    protected $table = 'baskets';

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public static function factory()
    {
        return BasketFactory::new();
    }
}