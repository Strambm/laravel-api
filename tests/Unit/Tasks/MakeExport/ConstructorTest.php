<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExport;

use Mockery;
use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Api\Tasks\MakeExport;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tasks\MakeExportParams;

class ConstructorTest extends TestCase
{
    #[Test]
    public function basic(): void
    {
        $controller = 'testController';
        $ids = [3, 8, 9];
        $returnUrl = 'tetgefve';
        $params = new MakeExportParams($controller, $ids, $returnUrl);
        $task = new MakeExport($params);
        $storedParams = Reflection::getHiddenProperty($task, 'params');
        $this->assertEquals($params, $storedParams);
    }

    #[Test]
    public function wakeup(): void
    {
        $controller = 'testController';
        $ids = [3, 8, 9];
        $returnUrl = 'tetgefve';
        $params = new MakeExportParams($controller, $ids, $returnUrl);
        $task = new MakeExport($params);
        $wakeUp = unserialize(serialize($task));
        $storedParams = Reflection::getHiddenProperty($wakeUp, 'params');
        $this->assertEquals($params, $storedParams);
    }

    protected function setUp(): void
    {
        parent::setUp();
        Auth::shouldReceive('check')
            ->once()
            ->andReturn(false);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }
}