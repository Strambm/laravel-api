<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExport;

use Mockery;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Api\Tasks\MakeExport;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tasks\SendMailParams;
use Cetria\Laravel\Api\Tasks\SendExportEmail;
use Cetria\Laravel\Api\Tasks\MakeExportParams;
use Cetria\Laravel\Api\Models\ExportDrivers\CSV;

class ContactUserTest extends TestCase
{
    #[Test]
    public function sendHttpRequest(): void
    {
        $this->mockHttpFacade('testURL', 'test-url.cz');
        $params = $this->getMockBuilder(MakeExportParams::class)
            ->onlyMethods(['getExportUrl'])
            ->setConstructorArgs(['', [], CSV::class, 'test-url.cz'])
            ->getMock();
        $params->expects($this->any())
            ->method('getExportUrl')
            ->willReturn('testURL');
        $filename = 'testfile.csv';
        
        $this->act($params, $filename);
    }

    #[Test]
    public function sendEmailExport(): void
    {
        $this->mockEmailFacade('testPath', 'b@b.b');

        $emailParams = new SendMailParams('b@b.b', DummyEmailRoute::class);
        $params = new MakeExportParams('', [], CSV::class, returnEmailsParams: $emailParams);
        $filename = 'testPath';

        $this->act($params, $filename);
    }

    #[Test]
    public function sendBoth(): void
    {
        $this->mockEmailFacade('testPath', 'b@b.b');
        $this->mockHttpFacade('testPath', 'test-url.cz');
        $emailParams = new SendMailParams('b@b.b', DummyEmailRoute::class);
        $params = $this->getMockBuilder(MakeExportParams::class)
            ->onlyMethods(['getExportUrl'])
            ->setConstructorArgs(['', [], CSV::class, 'test-url.cz', $emailParams])
            ->getMock();
        $params->expects($this->any())
            ->method('getExportUrl')
            ->willReturn('testPath');
        $filename = 'testPath';

        $this->act($params, $filename);
    }

    private function mockEmailFacade(string $exportPath, string $email): void
    {
        $storageMock = Mockery::mock('alias:Illuminate\Support\Facades\Mail');
        $storageMock->shouldReceive('to')
            ->once()
            ->withArgs(function (string $emailAddress) use ($email): bool {
                $this->assertEquals($email, $emailAddress);
                return true;
            })
            ->andReturnSelf();
        $storageMock->shouldReceive('send')
            ->once()
            ->withArgs(function(SendExportEmail $mail) use ($exportPath): bool {
                $this->assertEquals($exportPath, $mail->data['attachmentPath']);
                return true;
            });
    }

    private function mockHttpFacade(string $exportPath, string $pingUrl): void
    {
        $storageMock = Mockery::mock('alias:Illuminate\Support\Facades\Http');
        $storageMock->shouldReceive('post')
            ->once()
            ->withArgs(function (string $url, array $data) use ($pingUrl, $exportPath): bool {
                $this->assertEquals($pingUrl, $url);
                $this->assertEquals(['exportUrl' => $exportPath], $data);
                return true;
            });
    }

    private function act(MakeExportParams $params, string $filename): void
    {
        $job = new MakeExport($params);
        $methodName = 'contactUser';
        $method = Reflection::getHiddenMethod($job, $methodName);
        $method->invoke($job, $filename);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }
}