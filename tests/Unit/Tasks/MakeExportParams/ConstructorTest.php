<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams;

use Mockery;
use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tasks\SendMailParams;
use Cetria\Laravel\Api\Tasks\MakeExportParams;
use Cetria\Laravel\Api\Models\ExportDrivers\XML;

class ConstructorTest extends TestCase
{
    #[Test]
    public function constructor(): void
    {
        Auth::shouldReceive('check')
            ->once()
            ->andReturn(false);
        $controller = 'controller';
        $ids = [1, 2, 3];
        $returnUrl = 'localhost';

        $params = new MakeExportParams($controller, $ids, XML::class, $returnUrl, new SendMailParams('testEmail', 'testMailClass'));
        
        $this->assertEquals($ids, Reflection::getHiddenProperty($params, 'ids'));
        $this->assertEquals($controller, Reflection::getHiddenProperty($params, 'controller'));
        $this->assertEquals($returnUrl, Reflection::getHiddenProperty($params, 'returnUrl'));
        $this->assertEquals(XML::class, Reflection::getHiddenProperty($params, 'driver'));
        $sendEmailParams = Reflection::getHiddenProperty($params, 'returnEmailParams');
        $this->assertEquals('testEmail', Reflection::getHiddenProperty($sendEmailParams, 'email'));
        $this->assertEquals('testMailClass', Reflection::getHiddenProperty($sendEmailParams, 'emailClass'));
    }

    #[Test]
    public function authenticated(): void
    {
        $userKey = 1;
        Auth::shouldReceive('check')
            ->once()
            ->andReturn(true);
        $user = Mockery::mock('User');
        $user->shouldReceive('getKey')
            ->andReturn($userKey);
        $userClass = get_class($user);
        Auth::shouldReceive('user')
            ->andReturn($user);

        $makeExportParams = new MakeExportParams('App\Http\Controllers\YourController', [1, 2, 3], '/your-return-url');

        $this->assertEquals($userClass, Reflection::getHiddenProperty($makeExportParams, 'user_class'));
        $this->assertEquals($userKey, Reflection::getHiddenProperty($makeExportParams, 'user_key')); 
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }
}