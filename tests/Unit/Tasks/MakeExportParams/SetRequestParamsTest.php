<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Tasks\MakeExportParams;

class SetRequestParamsTest extends TestCase
{
    #[Test]
    public function array(): void
    {
        $paramData = ['test' => 'testData'];
        $result = $this->act($paramData);
        $this->assertEquals($paramData, Reflection::getHiddenProperty($result, 'requestParams'));
    }

    #[Test]
    public function emptyRequest(): void
    {
        $result = $this->act(new Request());
        $this->assertEquals([], Reflection::getHiddenProperty($result, 'requestParams'));
    }

    #[Test]
    public function request(): void
    {
        $requestData = ['test' => 'testData'];
        $result = $this->act(new Request([], $requestData));
        $this->assertEquals([], Reflection::getHiddenProperty($result, 'requestParams'));
    }

    public function arrangeParams(): MakeExportParams
    {
        /** @var MakeExportParams|MockObject $params */
        $params = $this->getMockBuilder(MakeExportParams::class)
            ->onlyMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        return $params;
    }

    protected function act(Request|array $data): MakeExportParams
    {
        $params = $this->arrangeParams();

        return $params->setRequestParams($data);
    }
}