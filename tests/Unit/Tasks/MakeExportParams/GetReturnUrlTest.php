<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Tasks\MakeExportParams;

class GetReturnUrlTest extends TestCase
{
    #[Test]
    public function getReturnUrl(): void
    {
        $url = 'randomUrl';
        $params = $this->arrangeParams($url);
        $result = $this->act($params);
        $this->assertEquals($url, $result);
    }

    protected function arrangeParams(string $returnUrl): MakeExportParams
    {
        /** @var MockObject|MakeExportParams $params */
        $params = $this->getMockBuilder(MakeExportParams::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        Reflection::setHiddenProperty($params, 'returnUrl', $returnUrl);
        return $params;
    }

    protected function act(MakeExportParams $params): string
    {
        return $params->getReturnUrl();
    }
}