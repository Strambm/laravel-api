<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams;

use PHPUnit\Framework\TestCase;
use Illuminate\Routing\Controller;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Tasks\MakeExportParams;

class GetControllerTest extends TestCase
{
    public static $increment = 1;
    protected static $controllerInstance;

    #[Test]
    public function cache(): void
    {
        $params = $this->arrangeParams();

        $result = $params->getController();
        $this->assertInstanceOf(get_class(static::$controllerInstance), $result);
        $cached = Reflection::getHiddenProperty($params, 'controllerInstance');
        $this->assertEquals($cached, $result);
    }

    #[Test]
    public function applyCache(): void
    {
        $this->arrangeController();
        $cachedInstance = static::$controllerInstance;
        $params = $this->arrangeParams();
        Reflection::setHiddenProperty($params, 'controllerInstance', $cachedInstance);

        $result = $params->getController();
        $this->assertEquals($cachedInstance->id, $result->id);
        $this->assertNotEquals($cachedInstance->id, static::$controllerInstance->id);
        $cached = Reflection::getHiddenProperty($params, 'controllerInstance');
        $this->assertEquals($cached->id, $cachedInstance->id);
    }

    protected function arrangeParams(): MakeExportParams|MockObject
    {
        $this->arrangeController();
        $params = $this->getMockBuilder(MakeExportParams::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        Reflection::setHiddenProperty($params, 'controller', get_class(static::$controllerInstance));
        return $params;
    }

    protected function arrangeController(): void
    {
        $controller = new class extends Controller
        {
            public $id;

            public function __construct()
            {
                $this->id = ++ GetControllerTest::$increment;
            }
        };
        static::$controllerInstance = $controller;
    }
}