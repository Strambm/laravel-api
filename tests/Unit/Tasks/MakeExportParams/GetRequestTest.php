<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams;

use Illuminate\Http\Request;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Tasks\MakeExportParams;

class GetRequestTest extends TestCase
{
    #[Test]
    #[DataProvider('basicDataProvider')]
    public function basic(array $requestParams): void
    {
        $params = $this->arrangeParams($requestParams);
        $result = $this->act($params);
        $this->assertEquals($requestParams, $result->all());
    }

    public static function basicDataProvider(): array
    {
        return [
            [
                []
            ], [
                ['test' => '84']
            ]
        ];
    }

    protected function arrangeParams(array $requestParams): MakeExportParams
    {
        /** @var MakeExportParams|MockObject $params */
        $params = $this->getMockBuilder(MakeExportParams::class)
            ->onlyMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        Reflection::setHiddenProperty($params, 'requestParams', $requestParams);
        return $params;
    }

    protected function act(MakeExportParams $params): Request
    {
        return $params->getRequest();
    }
}