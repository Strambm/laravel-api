<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams;

use PHPUnit\Framework\TestCase;
use Illuminate\Routing\Controller;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Tasks\MakeExportParams;

class GetItemsOnPageTest extends TestCase
{
    #[Test]
    public function getItemsOnPage(): void
    {
        $params = $this->arrangeParams();
        $result = $this->act($params);
        $this->assertEquals(1000, $result);
    }

    protected function arrangeParams(): MakeExportParams
    {
        $controller = new class extends Controller
        {
            protected $itemsOnPage = 1000;
        };
        /** @var MockObject|MakeExportParams $params */
        $params = $this->getMockBuilder(MakeExportParams::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getController'])
            ->getMock();
        $params->expects($this->any())
            ->method('getController')
            ->willReturn($controller);
        return $params;
    }

    protected function act(MakeExportParams $params): int
    {
        return $params->getItemsOnPage();
    }
}