<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams;

use PHPUnit\Framework\Attributes\DoesNotPerformAssertions;
use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Tasks\MakeExportParams;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthTest extends TestCase
{
    public static $user;

    #[Test]
    #[DoesNotPerformAssertions]
    public function testAuth(): void
    {
        $this->arrangeUserModel();
        $params = $this->arrangeParams();
        
        
        Auth::shouldReceive('setUser')
            ->once()
            ->with(static::$user);
        $this->act($params);
    }

    #[Test]
    public function userDoesntExist(): void
    {
        $this->arrangeUserModel();
        $params = $this->arrangeParams();
        $this->arrangeUserModel();

        $this->expectException(ModelNotFoundException::class);
        $this->act($params);
    }

    #[Test]
    #[DoesNotPerformAssertions]
    public function unauthenticable(): void
    {
        $params = $this->arrangeParams();

        $this->act($params);
    }

    protected function arrangeUserModel(): void
    {
        $user = new class() extends Model
        {
            protected $key;

            public function __construct($attributes = []) {
                parent::__construct($attributes);
                $this->key = random_int(1, 50);
            }

            public function getKey()
            {
                return $this->key;
            }

            public static function withoutGlobalScopes(): self
            {
                return AuthTest::$user;
            }

            public function findOrFail($key): static
            {
                if($key == $this->getKey()) {
                    return $this;
                } else {
                    throw new ModelNotFoundException();
                }
                
            }
        };
        static::$user = $user;
    }

    protected function arrangeParams(): MockObject|MakeExportParams
    {
        $params = $this->getMockBuilder(MakeExportParams::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        if(static::$user) {
            Reflection::setHiddenProperty($params, 'user_class', get_class(static::$user));
            Reflection::setHiddenProperty($params, 'user_key', static::$user->getKey());
        }
        return $params;
    }

    protected function act(MakeExportParams $params): void
    {
        $params->auth();
    }

    protected function tearDown(): void
    {
        static::$user = null;
        parent::tearDown();
    }
}