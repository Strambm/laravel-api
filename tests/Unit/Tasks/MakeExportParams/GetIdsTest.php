<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Tasks\MakeExportParams;

class GetIdsTest extends TestCase
{
    #[Test]
    public function getReturnUrl(): void
    {
        $ids = [1,2,3];
        $params = $this->arrangeParams($ids);
        $result = $this->act($params);
        $this->assertEquals($ids, $result);
    }

    protected function arrangeParams(array $ids): MakeExportParams
    {
        /** @var MockObject|MakeExportParams $params */
        $params = $this->getMockBuilder(MakeExportParams::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        Reflection::setHiddenProperty($params, 'ids', $ids);
        return $params;
    }

    protected function act(MakeExportParams $params): array
    {
        return $params->getIds();
    }
}