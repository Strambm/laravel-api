<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\SendMailParams;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tasks\SendMailParams;

class ConstructorTest extends TestCase
{
    #[Test]
    public function constructor(): void
    {
        $params = new SendMailParams('email@gmail.com', static::class);
        
        $this->assertEquals('email@gmail.com', Reflection::getHiddenProperty($params, 'email'));
        $this->assertEquals(static::class, Reflection::getHiddenProperty($params, 'emailClass'));
    }
}