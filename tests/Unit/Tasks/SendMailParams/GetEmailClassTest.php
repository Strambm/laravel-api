<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\SendMailParams;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tasks\SendMailParams;
use PHPUnit\Framework\MockObject\MockObject;

class GetEmailClassTest extends TestCase
{
    #[Test]
    public function getReturnUrl(): void
    {
        $emailClass = static::class;
        $params = $this->arrangeParams($emailClass);
        $result = $this->act($params);
        $this->assertEquals($emailClass, $result);
    }

    protected function arrangeParams(string $emailClass): SendMailParams
    {
        /** @var MockObject|SendMailParams $params */
        $params = $this->getMockBuilder(SendMailParams::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        Reflection::setHiddenProperty($params, 'emailClass', $emailClass);
        return $params;
    }

    protected function act(SendMailParams $params): string
    {
        return $params->getEmailClass();
    }
}