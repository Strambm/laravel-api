<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Tasks\SendMailParams;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tasks\SendMailParams;
use PHPUnit\Framework\MockObject\MockObject;

class GetEmailTest extends TestCase
{
    #[Test]
    public function getReturnUrl(): void
    {
        $email = 'a@a.com';
        $params = $this->arrangeParams($email);
        $result = $this->act($params);
        $this->assertEquals($email, $result);
    }

    protected function arrangeParams(string $email): SendMailParams
    {
        /** @var MockObject|SendMailParams $params */
        $params = $this->getMockBuilder(SendMailParams::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
        Reflection::setHiddenProperty($params, 'email', $email);
        return $params;
    }

    protected function act(SendMailParams $params): string
    {
        return $params->getEmail();
    }
}