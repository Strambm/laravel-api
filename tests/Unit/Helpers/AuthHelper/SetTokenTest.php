<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Helpers\AuthHelper;

use Illuminate\Support\Str;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Cetria\Helpers\Reflection\Reflection;

class SetTokenTest extends TestCase
{
    #[Test]
    public function testMethod(): void
    {
        $token = Str::random();
        $this->assertNull(Reflection::getHiddenProperty(AuthHelper::class, 'token'));
        $this->act($token);
        $this->assertEquals($token, Reflection::getHiddenProperty(AuthHelper::class, 'token'));
    }

    protected function act(string $value): void
    {
        AuthHelper::setToken($value);
    }

    protected function tearDown(): void
    {
        Reflection::setHiddenProperty(AuthHelper::class, 'token', null);
        parent::tearDown();
    }
}