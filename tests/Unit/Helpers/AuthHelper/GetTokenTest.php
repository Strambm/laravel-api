<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Helpers\AuthHelper;

use Illuminate\Support\Str;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Cetria\Helpers\Reflection\Reflection;

class GetTokenTest extends TestCase
{
    #[Test]
    public function testMethod(): void
    {
        $token = $this->act();
        $this->assertIsValidToken($token);

    }

    protected function act(): string
    {
        $token = AuthHelper::getToken();
        return $token;
    }

    protected function assertIsValidToken(string $token): void
    {
        $pattern = '/^Bearer [a-zA-Z0-9]+$/';
        $isValidToken = preg_match($pattern, $token) === 1;
        $this->assertTrue($isValidToken);
    }

    protected function setUp(): void
    {
        parent::setUp();
        Reflection::setHiddenProperty(AuthHelper::class, 'token', Str::random());
    }

    protected function tearDown(): void
    {
        Reflection::setHiddenProperty(AuthHelper::class, 'token', null);
        parent::tearDown();
    }
}