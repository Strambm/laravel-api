<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Helpers\AuthHelper;

use Illuminate\Support\Str;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Cetria\Helpers\Reflection\Reflection;

class LogoutTest extends TestCase
{
    #[Test]
    public function testMethod(): void
    {
        $this->act();
        $this->assertNull(Reflection::getHiddenProperty(AuthHelper::class, 'token'));
    }

    protected function setUp(): void
    {
        parent::setUp();
        Reflection::setHiddenProperty(AuthHelper::class, 'token', Str::random());
    }

    protected function act(): void
    {
        AuthHelper::logout();
    }

    protected function tearDown(): void
    {
        Reflection::setHiddenProperty(AuthHelper::class, 'token', null);
        parent::tearDown();
    }
}