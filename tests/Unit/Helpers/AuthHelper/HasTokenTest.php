<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Helpers\AuthHelper;

use Illuminate\Support\Str;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Cetria\Helpers\Reflection\Reflection;

class HasTokenTest extends TestCase
{
    #[Test]
    public function testMethod__false(): void
    {
        $result = $this->act();
        $this->assertFalse($result);
    }

    #[Test]
    public function testMethod__true(): void
    {
        Reflection::setHiddenProperty(AuthHelper::class, 'token', Str::random());
        $result = $this->act();
        $this->assertTrue($result);
    }

    protected function act(): bool
    {
        return AuthHelper::hasToken();
    }

    protected function tearDown(): void
    {
        Reflection::setHiddenProperty(AuthHelper::class, 'token', null);
        parent::tearDown();
    }
}