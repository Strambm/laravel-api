<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Helpers;

use Cetria\Laravel\Api\Traits\Tests\Helpers;

class HelpersObject
{
    use Helpers;

    protected function getBasicRouteName(): string
    {
        return '';
    }

    public function json($method, $uri, array $data = [], array $headers = [])
    {

    }

    public static function assertEquals($expected, $actual, string $message = ''): void
    {
        $trace = debug_backtrace();
        $self = $trace[1]['object'];
        $self->mockableAssertEquals($expected, $actual, $message);
    }

    protected function mockableAssertEquals($expected, $actual, string $message = ''): void
    {

    }

    public static function assertNotNull($actual, string $message = ''): void
    {

    }

    protected function getControllerClass(): string
    {
        return '';
    }
}