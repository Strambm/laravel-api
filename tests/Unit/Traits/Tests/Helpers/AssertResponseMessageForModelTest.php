<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Helpers;

use JsonSerializable;
use PHPUnit\Framework\TestCase;
use Illuminate\Support\Collection;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;

class AssertResponseMessageForModelTest extends TestCase
{  
    private $equalsBuffer = [];

    #[Test]
    public function compareCollection(): void
    {
        $responseModel = [
            'a' => [
                '1',
                'c'
            ],
            'b' => [
                'test',
                'fail'
            ],
            'id' => 1
        ];
        $model = new class() extends Model {};
        $model->setRelation('a', new Collection([
            new class() implements JsonSerializable {public function __tostring(){return '1';} public function jsonSerialize(): string { return (string)$this; }}, 
            new class() implements JsonSerializable {public function __tostring(){return 'c';} public function jsonSerialize(): string { return (string)$this; }},
        ]));
        $this->equalsBuffer[] = true;
        $model->setRelation('b', new Collection(['test', 'test']));
        $this->equalsBuffer[] = false;

        $this->handleTest($responseModel, $model);
    }

    #[Test]
    public function compareArray(): void
    {
        $responseModel = [
            'a' => [
                '1',
                'c'
            ],
            'b' => [
                'test',
                'fail'
            ],
            'id' => 1
        ];
        $model = new class() extends Model {};
        $model->a = [
            '1',
            'c'
        ];
        $this->equalsBuffer[] = true;
        $model->b = [
            'test',
            'test'
        ];
        $this->equalsBuffer[] = false;

        $this->handleTest($responseModel, $model);
    }

    #[Test]
    public function compareEnum(): void
    {
        $responseModel = [
            'a' => EnumWithValue::Val1->value,
            'b' => EnumWithValue::Val1->value,
            'id' => 1
        ];
        $model = new class() extends Model {};
        $model->a = EnumWithValue::Val1;
        $this->equalsBuffer[] = true;
        $model->b = EnumWithValue::Val2;
        $this->equalsBuffer[] = false;

        $this->handleTest($responseModel, $model);
    }

    #[Test]
    public function compareBasicDateType(): void
    {
        $responseModel = [
            'a' => 1.10,
            'b' => 2,
            'c' => 'test',
            'id' => 1
        ];
        $model = new class() extends Model {};
        $model->a = 1.10;
        $this->equalsBuffer[] = true;
        $model->b = 2;
        $this->equalsBuffer[] = true;
        $model->c = 'test';
        $this->equalsBuffer[] = true;

        $this->handleTest($responseModel, $model);
    }

    private function handleTest(array $responseModel, Model $model): void
    {
        $this->act($responseModel, $model);
        $this->assertCount(0, $this->equalsBuffer);
    }

    private function act(array $responseModel, Model $model): void
    {
        $helpersObject = $this->arrangeHelpersObject();
        $methodName = 'assertResponseMessageForModel';
        $method = Reflection::getHiddenMethod($helpersObject, $methodName);
        $method->invoke($helpersObject, $responseModel, $model);
    }

    private function arrangeHelpersObject(): HelpersObject
    {
        /** @var HelpersObject|MockObject $helpersObject */
        $helpersObject = $this->getMockBuilder(HelpersObject::class)
            ->onlyMethods([
                'mockableAssertEquals',
                'assertArrayHasSameValues',
            ])
            ->getMock();
        $helpersObject->expects($this->any())
            ->method('mockableAssertEquals')
            ->willReturnCallback(function($expectedValue, $value): void {
                $isEquals = array_shift($this->equalsBuffer);
                if($isEquals) {
                    $this->assertEquals($expectedValue, $value);
                } else {
                    $this->assertNotEquals($expectedValue, $value);
                }
            });
        return $helpersObject;
    }
}