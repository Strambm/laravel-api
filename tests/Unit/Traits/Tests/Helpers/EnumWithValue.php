<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Helpers;

enum EnumWithValue: int
{
    case Val1 = 1;
    case Val2 = 2;
}