<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Helpers;

use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Illuminate\Contracts\Auth\Authenticatable;

class LoginTest extends TestCase
{  
    private $authUser = null;

    #[Test]
    public function loginWithoutToken(): void
    {
        $authUser = new AuthenticatableWithoutTokens();
        $authUser->id = 54;
        $this->mockLogForUser($authUser);
        

        $this->act($authUser);

        $this->assertFalse(AuthHelper::hasToken());
        $this->assertEquals($authUser->id, $this->authUser->id);
    }

    #[Test]
    public function loginWithToken(): void
    {
        $authUser = new AuthenticatableWithTokens();
        $authUser->id = 53;
        $this->mockLogForUser($authUser);

        $this->act($authUser);

        $this->assertEquals(AuthHelper::getToken(), 'Bearer plaintext');
        $this->assertEquals($authUser->id, $this->authUser->id);
    }

    private function mockLogForUser(Authenticatable $authenticatable): void
    {
        Auth::shouldReceive('login')
        ->once()
        ->with($authenticatable)
        ->andReturnUsing(function (Authenticatable $user): void  {
            $this->authUser = $user;
        });
    }

    private function act(Authenticatable $authenticatable): void
    {
        $object = new HelpersObject();
        $methodName = 'login';
        $method = Reflection::getHiddenMethod($object, $methodName);
        $method->invoke($object, $authenticatable);
    }
}