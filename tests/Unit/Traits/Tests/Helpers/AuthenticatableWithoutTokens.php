<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Helpers;

use Cetria\Laravel\Api\Tests\Dummy\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as UseAuthenticatable;

class AuthenticatableWithoutTokens extends User
implements Authenticatable
{
    use UseAuthenticatable;
}