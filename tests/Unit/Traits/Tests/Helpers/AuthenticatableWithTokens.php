<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Helpers;

use Cetria\Laravel\Api\Tests\Dummy\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as UseAuthenticatable;
use stdClass;

class AuthenticatableWithTokens extends User
implements Authenticatable
{
    use UseAuthenticatable;

    public function createToken(string $key): object
    {
        $class = new class('plaintext') {
            public $plainTextToken = null;

            public function __construct(string $plaintext)
            {
                $this->plainTextToken = $plaintext;
            }
        };
        return $class;
    }
}