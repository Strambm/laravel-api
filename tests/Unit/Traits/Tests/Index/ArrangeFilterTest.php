<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Index;

use Exception;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Models\Params;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Collection;

class ArrangeFilterTest extends TestCase
{
    #[Test]
    public function leastTwoTestInstances(): void
    {
        $this->expectException(Exception::class);
        $collection = new Collection();
        $collection->push($this->arrangeModelInstance(['id' => 1]));

        $params = new Params();

        $this->act($params, $collection);
    }

    #[Test]
    public function sameModelsInCollection(): void
    {
        $this->expectException(Exception::class);
        $collection = new Collection();
        $collection->push($this->arrangeModelInstance(['id' => 1]));
        $collection->push($this->arrangeModelInstance(['id' => 1]));

        $params = new Params();

        $this->act($params, $collection);
    }

    #[Test]
    public function correct(): void
    {
        $collection = new Collection();
        $collection->push($this->arrangeModelInstance(['id' => 1]));
        $collection->push($this->arrangeModelInstance(['id' => 2]));

        $params = new Params();

        /** @var Params $result */
        $result = $this->act($params, $collection);
        $this->assertEquals(1, $result->getVissibleCount());
        $this->assertCount(1, $result->getRouteParams());
        $this->assertArrayHasKey('filter', $result->getRouteParams());
    }

    private function arrangeModelInstance(array $attributes = []): Model
    {
        $model = new class extends Model
        {

        };
        $model->setRawAttributes($attributes);
        return $model;
    }

    protected function arrangeTrait()
    {
        $trait = new IndexObject();
        return $trait;
    }

    protected function act(Params $params, Collection $models): Params
    {
        $trait = $this->arrangeTrait();
        $methodName = 'arrangeFilter';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        $result = $method->invoke($trait, $params, $models);
        return $result;
    }
}