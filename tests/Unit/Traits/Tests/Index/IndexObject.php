<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Index;

use Cetria\Laravel\Api\Traits\Tests\Index;
use PHPUnit\Framework\MockObject\MockBuilder;

class IndexObject
{
    use Index;

    protected function getActualRouteParamName(): string
    {

    }

    protected function getControllerClass(): string
    {

    }

    protected function getMockBuilder(string $className): MockBuilder
    {

    }

    protected function getParrentModelClass(): string
    {

    }

    protected function loginTestUserIfCan(): void
    {

    }

    public static function assertEquals($expected, $actual, string $message = ''): void
    {

    }

    public static function assertNotNull($actual, string $message = ''): void
    {

    }

    protected function getBasicRouteName(): string
    {

    }

    public function json($method, $uri, array $data = [], array $headers = [])
    {

    }
}