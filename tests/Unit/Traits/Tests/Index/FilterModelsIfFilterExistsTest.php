<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Index;

use Exception;
use PHPUnit\Framework\Attributes\DoesNotPerformAssertions;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Filter;
use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Api\Models\Params;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Collection;
use PHPUnit\Framework\MockObject\MockObject;

class FilterModelsIfFilterExistsTest extends TestCase
{
    #[Test]
    public function withoutChange(): void
    {
        $this->expectException(Exception::class);
        $collection = new Collection();
        $collection->push($this->arrangeModel(['param' => 1]));
        $collection->push($this->arrangeModel(['param' => 2]));
        $params = $this->arrangeParams(new Condition('param', Operator::LT, 3));

        $this->act($params, $collection);
    }

    #[Test]
    #[DoesNotPerformAssertions]
    public function withoutFilterParam(): void
    {
        /** @var Collection|MockObject $collection */
        $collection = $this->getMockBuilder(Collection::class)
            ->onlyMethods(['where'])
            ->getMock();
        $collection->expects($this->any())
            ->method('where')
            ->willThrowException(new Exception('Used Where function'));
        $collection->push($this->arrangeModel(['param' => 1]));
        $collection->push($this->arrangeModel(['param' => 2]));

        $this->act(new Params(), $collection);
    }

    #[Test]
    public function filter(): void
    {
        $collection = new Collection();
        $collection->push($this->arrangeModel(['param' => 1]));
        $collection->push($this->arrangeModel(['param' => 2]));
        $params = $this->arrangeParams(new Condition('param', Operator::LT, 2));

        $result = $this->act($params, $collection);
        $this->assertCount(1, $result);
        $this->assertEquals(1, $result->first()->param);
    }

    protected function arrangeModel(array $attributes = []): Model
    {
        $model = new class extends Model
        {

        };
        $model->setRawAttributes($attributes);
        return $model;
    }

    protected function arrangeParams(Condition $condition): Params
    {
        $filter = new Filter();
        $filter->addScope($condition);
        $params = new Params();
        $params->addRouteParam('filter', (string) $filter);
        return $params;
    }

    protected function act(Params $params, Collection $models): Collection
    {
        $trait = new IndexObject();
        $methodName = 'filterModelsIfFilterExists';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        return $method->invoke($trait, $params, $models);
    }
}