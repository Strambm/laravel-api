<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Arranges;

use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Models\Params;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tests\Dummy\Basket;
use Cetria\Laravel\Api\Tests\Dummy\Product;
use Illuminate\Database\Eloquent\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Traits\Tests\Arranges;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;

class ArrangeVissiblesTest extends TestCase
{
    #[Test]
    public function basicCrud(): void
    {
        $params = (new Params())
            ->setVissibleCount(4);
        $trait = $this->arrangeBasicCrudTrait();
        $result = $this->act($trait, $params);
        $this->assertCount(4, $result);
        $this->assertEquals(4, ($this->getParrentModelClass())::query()->count());
        $this->assertArrayHasKey($this->getRouteName(), $params->getRouteParams());
        $this->assertInstanceOf($this->getArrangedClass($this->getParrentModelClass()), $result->first());
    }

    #[Test]
    public function multidimensionalCrud(): void
    {
        $params = (new Params())
            ->setVissibleCount(2);
        $trait = $this->arrangeMultidimensionalCrudTrait();
        $result = $this->act($trait, $params);
        $this->assertCount(2, $result);
        $this->assertEquals(2, ($this->getMultidimensionalModelClass())::query()->count());
        $this->assertEquals(1, ($this->getParrentModelClass())::query()->count());
        $this->assertArrayHasKey($this->getRouteName(), $params->getRouteParams());
        $this->assertArrayHasKey($this->getMultidimensionalRouteName(), $params->getRouteParams());
        $this->assertInstanceOf($this->getArrangedClass($this->getMultidimensionalModelClass()), $result->first());
    }

    private function arrangeBasicCrudTrait(): MockObject
    {
        $trait = $this->getMockBuilder(ArrangeObject::class)
            ->onlyMethods(['isTestedControllerMultidimensional', 'loginTestUserIfCan', 'getParrentModelClass', 'getActualRouteParamName', 'getParamsForCorrectGenerate'])
            ->getMock();
        $trait->expects($this->any())
            ->method('isTestedControllerMultidimensional')
            ->willReturn(false);
        $trait->expects($this->any())
            ->method('getParrentModelClass')
            ->willReturn($this->getParrentModelClass());
        $trait->expects($this->any())
            ->method('getActualRouteParamName')
            ->willReturn($this->getRouteName());
        $trait->expects($this->any())
            ->method('getParamsForCorrectGenerate')
            ->willReturn((new Params()));
        return $trait;
    }

    private function arrangeMultidimensionalCrudTrait(): MockObject
    {
        $trait = $this->getMockBuilder(ArrangeObject::class)
            ->onlyMethods(['isTestedControllerMultidimensional', 'loginTestUserIfCan', 'getParrentModelClass', 'getActualRouteParamName', 'getTestControllerBeforeInstance'])
            ->getMock();
        $trait->expects($this->any())
            ->method('isTestedControllerMultidimensional')
            ->willReturn(true);
        $trait->expects($this->any())
            ->method('getParrentModelClass')
            ->willReturn($this->getMultidimensionalModelClass());
        $trait->expects($this->any())
            ->method('getActualRouteParamName')
            ->willReturn($this->getMultidimensionalRouteName());
        $trait->expects($this->any())
            ->method('getTestControllerBeforeInstance')
            ->willReturn($this->arrangeBasicCrudTrait());
        return $trait;
    }

    private function getParrentModelClass(): string
    {
        return Product::class;
    }

    private function getMultidimensionalModelClass(): string
    {
        return Basket::class;
    }

    private function getRouteName(): string
    {
        return 'testRouteName1';
    }

    private function getMultidimensionalRouteName(): string
    {
        return 'testRouteName2';
    }

    private function act(MockObject $trait, Params &$params): Collection
    {
        $methodName = 'arrangeVissibles';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        $result = $method->invokeArgs($trait, [&$params]);
        return $result;
    }

    private function getArrangedClass(string $sourceclass): string
    {
        $arranged = $sourceclass::factory()->make();
        return get_class($arranged);
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init(); 
    }
}