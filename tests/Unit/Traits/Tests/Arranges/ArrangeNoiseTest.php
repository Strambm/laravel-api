<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Arranges;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Models\Params;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Api\Tests\Dummy\User;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;

class ArrangeNoiseTest extends TestCase
{
    #[Test]
    #[DataProvider('basicDataProviderMethod')]
    public function basic(Params $params): void
    {
        $this->act($params);
        $this->assertEquals($params->getNoiseCount(), User::query()->count());
    }

    public static function basicDataProviderMethod(): array
    {
        return [
            [
                (new Params())->setNoiseCount(2)
            ], [
                (new Params())->setNoiseCount(8)
            ], [
                (new Params())->setNoiseCount(0)
            ]
        ];
    }

    private function act(Params $params): void
    {
        $trait = $this->arrangeTrait();
        $methodName = 'arrangeNoise';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        $method->invoke($trait, $params);
    }

    private function arrangeTrait(): MockObject
    {
        $trait = $this->getMockBuilder(ArrangeObject::class)
            ->onlyMethods(['getParrentModelClass','loginTestUserIfCan'])
            ->getMock();
        $trait->expects($this->any())
            ->method('getParrentModelClass')
            ->willReturn(User::class);
        return $trait;
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init(); 
    }
}