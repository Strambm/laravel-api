<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Arranges;

use Cetria\Laravel\Api\Models\Params;
use Cetria\Laravel\Api\Traits\Tests\Arranges;
use PHPUnit\Framework\MockObject\MockBuilder;

class ArrangeObject
{
    use Arranges;

    public function getMockBuilder(string $className): MockBuilder
    {

    }

    protected function getParrentModelClass(): string
    {

    }

    protected function getControllerClass(): string
    {

    }

    protected function getActualRouteParamName(): string
    {

    }

    public function getParamsForCorrectGenerate(): Params
    {
        
    }

    protected function loginTestUserIfCan(): void
    {
        
    }
}