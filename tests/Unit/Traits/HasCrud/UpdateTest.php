<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;

class UpdateTest extends TestCase
{
    #[Test]
    public function basic(): void
    {
        $model = Product::factory()->create(['name' => '', 'url' => '', 'price' => 1]);
        $trat = $this->arrangeTrait(get_class($model));
        $resut = $trat->update(new Request(['name' => 'name', 'url' => 'url', 'price' => 452]), $model->getKey());
        $this->assertEquals(Response::HTTP_OK, $resut->getStatusCode());
        $content = json_decode($resut->getContent(), true)['message'];
        $this->assertEquals($model->id, $content['id']);
        $this->assertEquals('name', $content['name']);
        $this->assertEquals('url', $content['url']);
        $this->assertEquals(452, $content['price']);
        $stored = Product::findOrFail($model->getKey());
        $this->assertEquals('name', $stored->name);
        $this->assertEquals('url', $stored->url);
        $this->assertEquals(452, $stored->price);
    }

    /**
     * @return CrudControllerInterface
     */
    protected function arrangeTrait(string $className)
    {
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudObject::class)
            ->onlyMethods(['_getModelClass', 'updateVerify'])
            ->getMock();
        $trait->expects($this->any())
            ->method('_getModelClass')
            ->willReturn($className);
        return $trait;
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init(); 
    }
}