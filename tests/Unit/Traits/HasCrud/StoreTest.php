<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Exception;
use Throwable;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Laravel\Api\Tests\Dummy\User;
use Cetria\Laravel\Api\Tests\Dummy\Order;
use Cetria\Laravel\Api\Tests\Dummy\Basket;
use Cetria\Laravel\Api\Tests\Dummy\Product;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;

class StoreTest extends TestCase
{
    #[Test]
    public function transactionFailed(): void
    {
        $this->expectException(Exception::class);
        $trait = $this->arrangeTraitForTransactionFailed();
        try{
            $response = $trait->store(new Request(['name' => 'testName']));
        } catch(Exception $e) {
            $this->assertEquals($e->getMessage(), 'test');
            $this->assertEquals(0, User::count());
            throw $e;
        }
    }

    /**
     * @return CrudControllerInterface
     */
    protected function arrangeTraitForTransactionFailed()
    {
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudObject::class)
            ->onlyMethods(['createHasOneOrManyRelatedModels', 'getEmptyModelInstance', 'storeVerify'])
            ->getMock();
        $trait->expects($this->any())
            ->method('createHasOneOrManyRelatedModels')
            ->willThrowException(new Exception('test'));
        $trait->expects($this->any())
            ->method('getEmptyModelInstance')
            ->willReturn(new User());
        return $trait;
    }

    #[Test]
    public function basic(): void
    {
        $userName = 'testName';
        $trait = $this->arrangeTrait(new User());
        $result = $trait->store(new Request(['name' => $userName]));
        $this->assertEquals(Response::HTTP_CREATED, $result->getStatusCode());
        $content = json_decode($result->getContent(), true)['message'];
        $this->assertEquals($userName, $content['name']);
        $this->assertArrayHasKey('id', $content);
    }

    #[Test]
    public function withBelongsToRelation(): void
    {
        $date = Carbon::today();
        $userName = Str::random(5);
        $trait = $this->arrangeTrait($this->arrangeEmptyModelInstance(Order::class, ['user' => User::class]));
        $result = $trait->store(new Request([
            'date' => Carbon::today(),
            'user.name' => $userName,
        ]));
        $this->assertEquals(Response::HTTP_CREATED, $result->getStatusCode());
        $content = json_decode($result->getContent(), true)['message'];
        $this->assertEquals($userName, $content['user']['name']);
        $this->assertArrayHasKey('id', $content['user']);
        $this->assertEquals($date->format('Y-m-d H:i:s'), $content['date']);
        $this->assertArrayHasKey('id', $content);
    }

    #[Test]
    public function withBelongsToRelationTransactionRollback(): void
    {
        $userName = Str::random(5);
        $trait = $this->arrangeTraitWhereThrowSave();
        try {
            $failled = false;
            $trait->store(new Request([
                'user.name' => $userName,
            ]));
        } catch(Throwable $e) {
            $failled = true;
        }
        $this->assertTrue($failled);
        $this->assertEquals(0, User::withoutGlobalScopes()->count());
    }

    private function arrangeTraitWhereThrowSave(): MockObject
    {
        /** @var Model|MockObject $failingSaveMock */
        $failingSaveMock = new class extends Model
        {
            public function getToFormRelatedModels(): array
            {
                return [
                    'user' => User::class,
                ];
            }

            public function save(array $options = []): Model
            {
                throw new Exception('mockThrowing from method \'arrangeTraitWhereThrowSave\'');
            }

            public function user(): BelongsTo
            {
                return $this->belongsTo(User::class);
            }
        };
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudObject::class)
            ->onlyMethods(['getEmptyModelInstance', 'storeVerify'])
            ->getMock();
        $trait->expects($this->any())
            ->method('getEmptyModelInstance')
            ->willReturn($failingSaveMock);
        return $trait;
    }

    #[Test]
    public function withHasOneRelation(): void
    {
        $name = 'name';
        $url = 'url';
        $price = 152;
        $basketPrice = 178;
        $basketAmount = 8;
        $basketObjectId = 1;
        $basketObjectType = 'tmp';
        $trait = $this->arrangeTrait($this->arrangeEmptyModelInstance(Product::class, ['basketItemHasOne' => Basket::class]));
        $result = $trait->store(new Request([
            'name' => $name,
            'url' => $url,
            'price' => $price,
            'basketItemHasOne.price' => $basketPrice,
            'basketItemHasOne.amount' => $basketAmount,
            'basketItemHasOne.object_id' => $basketObjectId,
            'basketItemHasOne.object_type' => $basketObjectType,
        ]));
        $this->assertEquals(Response::HTTP_CREATED, $result->getStatusCode());
        $content = json_decode($result->getContent(), true)['message'];
        $this->assertEquals($name, $content['name']);
        $this->assertEquals($url, $content['url']);
        $this->assertEquals($price, $content['price']);
        $this->assertArrayHasKey('id', $content);
        $this->assertEquals($basketPrice, $content['basket_item_has_one']['price']);
        $this->assertEquals($basketAmount, $content['basket_item_has_one']['amount']);
        $this->assertEquals($basketObjectId, $content['basket_item_has_one']['object_id']);
        $this->assertEquals($basketObjectType, $content['basket_item_has_one']['object_type']);
        $this->assertArrayHasKey('id', $content['basket_item_has_one']);
    }

    #[Test]
    public function withHasOneRelationTransactionRollback(): void
    {
        $name = 'name';
        $url = 'url';
        $price = 152;
        $basketPrice = 178;
        $basketAmount = 8;
        $basketObjectId = 1;
        $trait = $trait = $this->arrangeTrait($this->arrangeEmptyModelInstance(Product::class, ['basketItemHasOne' => Basket::class]));;
        try {
            $failled = false;
            $trait->store(new Request([
                'name' => $name,
                'url' => $url,
                'price' => $price,
                'basketItemHasOne.price' => $basketPrice,
                'basketItemHasOne.amount' => $basketAmount,
                'basketItemHasOne.object_id' => $basketObjectId,
            ]));
        } catch(Throwable $e) {
            $failled = true;
        }
        $this->assertTrue($failled);
        $this->assertEquals(0, Product::withoutGlobalScopes()->count());
        $this->assertEquals(0, Basket::withoutGlobalScopes()->count());
    }

    #[Test]
    public function withHasManyRelation(): void
    {
        $name = 'name';
        $url = 'url';
        $price = 152;
        $basketPrice = 178;
        $basketAmount = 8;
        $basketObjectId = 1;
        $basketObjectType = 'tmp';
        $trait = $this->arrangeTrait($this->arrangeEmptyModelInstance(Product::class, ['basketItemsHasMany' => Basket::class]));
        $result = $trait->store(new Request([
            'name' => $name,
            'url' => $url,
            'price' => $price,
            'basketItemsHasMany.price' => [$basketPrice, $basketPrice],
            'basketItemsHasMany.amount' => [$basketAmount, $basketAmount],
            'basketItemsHasMany.object_id' => [$basketObjectId, $basketObjectId],
            'basketItemsHasMany.object_type' => [$basketObjectType, $basketObjectType],
        ]));
        $this->assertEquals(Response::HTTP_CREATED, $result->getStatusCode());
        $content = json_decode($result->getContent(), true)['message'];
        $this->assertEquals($name, $content['name']);
        $this->assertEquals($url, $content['url']);
        $this->assertEquals($price, $content['price']);
        $this->assertArrayHasKey('id', $content);
        $this->assertCount(2, $content['basket_items_has_many']);
        $this->assertNotEquals($content['basket_items_has_many'][0]['id'], $content['basket_items_has_many'][1]['id']);
    }

    #[Test]
    public function withHasManyRelationTransactionRollback(): void
    {
        $name = 'name';
        $url = 'url';
        $price = 152;
        $basketPrice = 178;
        $basketAmount = 8;
        $basketObjectId = 1;
        $basketObjectType = 'tmp';
        $trait = $trait = $this->arrangeTrait($this->arrangeEmptyModelInstance(Product::class, ['basketItemsHasMany' => Basket::class]));;
        try {
            $failled = false;
            $trait->store(new Request([
                'name' => $name,
                'url' => $url,
                'price' => $price,
                'basketItemsHasMany.price' => [$basketPrice],
                'basketItemsHasMany.amount' => [$basketAmount, $basketAmount],
                'basketItemsHasMany.object_id' => [$basketObjectId, $basketObjectId],
                'basketItemsHasMany.object_type' => [$basketObjectType, $basketObjectType],
            ]));
        } catch(Throwable $e) {
            $failled = true;
        }
        $this->assertTrue($failled);
        $this->assertEquals(0, Product::withoutGlobalScopes()->count());
        $this->assertEquals(0, Basket::withoutGlobalScopes()->count());
    }

    #[Test]
    public function withBelongsToRelationButUnused(): void
    {
        $date = Carbon::today();
        $trait = $this->arrangeTrait($this->arrangeEmptyModelInstance(Order::class, ['user' => User::class]));
        $user = User::factory()->create();
        $result = $trait->store(new Request([
            'date' => $date,
            'user_id' => $user->id,
        ]));
        $this->assertEquals(Response::HTTP_CREATED, $result->getStatusCode());
        $content = json_decode($result->getContent(), true)['message'];
        $this->assertEquals($user->name, $content['user']['name']);
        $this->assertEquals($user->id, $content['user']['id']);
    }

    protected function arrangeEmptyModelInstance(string $class, array $expectedToFormRelatedModels): Model
    {
        $model = $this->getMockBuilder($class)
            ->onlyMethods(['getToFormRelatedModels'])
            ->getMock();
        $model->expects($this->any())
            ->method('getToFormRelatedModels')
            ->willReturn($expectedToFormRelatedModels);
        return $model;
    }

    /**
     * @return CrudControllerInterface
     */
    protected function arrangeTrait(Model $getEmptyInstanceResult)
    {
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudObject::class)
            ->onlyMethods(['getEmptyModelInstance', 'storeVerify'])
            ->getMock();
        $trait->expects($this->any())
            ->method('getEmptyModelInstance')
            ->willReturn($getEmptyInstanceResult);
        return $trait;
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init(); 
    }
}