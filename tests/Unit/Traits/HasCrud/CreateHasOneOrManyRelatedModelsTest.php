<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tests\Dummy\Basket;
use Cetria\Laravel\Api\Tests\Dummy\Product;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;

class CreateHasOneOrManyRelatedModelsTest extends TestCase
{
    #[Test]
    public function hasOne(): void
    {
        $request = $this->arrangeRequestHasOne();
        $model = $this->arrangeSourceModel(['basketItemHasOne']);
        $trait = $this->arrangeTrait();
        $resultModel = $this->act($trait, $model, $request);
        $this->assertEquals(1, Basket::where('product_id', $model->id)->count());
        $this->assertNotNull($resultModel->getRelation('basketItemHasOne'));
    }

    #[Test]
    public function hasMany(): void
    {
        $request = $this->arrangeRequestHasMany(3);
        $model = $this->arrangeSourceModel(['basketItemsHasMany']);
        $trait = $this->arrangeTrait();
        $resultModel = $this->act($trait, $model, $request);
        $this->assertEquals(3, Basket::where('product_id', $model->id)->count());
        $this->assertCount(3, $resultModel->getRelation('basketItemsHasMany'));
    }

    protected function arrangeSourceModel(array $getToFormRelatedModelsResult): Model
    {
        $model = new Product(Product::factory()->make()->getAttributes());
        $model->save();
        Reflection::setHiddenProperty($model, 'toFormRelatedModels', $getToFormRelatedModelsResult);
        return $model;
    }

    protected function arrangeRequestHasOne(): Request
    {
        $params = [];
        foreach(Basket::factory()->make()->getAttributes() as $key => $value) {
            $params['basketItemHasOne.' . $key] = $value;
        }
        return new Request($params);
    }

    protected function arrangeRequestHasMany(int $count): Request
    {
        $params = [];
        for($counter = 0; $counter < $count; $counter ++) {
            foreach(Basket::factory()->make()->getAttributes() as $key => $value) {
                $attrKey = 'basketItemsHasMany.' . $key;
                $oldVal = [];
                if(array_key_exists($attrKey, $params)) {
                    $oldVal = $params[$attrKey];
                }
                $oldVal[$counter] = $value;
                $params['basketItemsHasMany.' . $key] = $oldVal;
            }
        }
        return new Request($params);
    }

    protected function act($trait, Model $model, Request $request): Model
    {
        $methodName = 'createHasOneOrManyRelatedModels';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        $method->invokeArgs($trait, [&$model, $request]);
        return $model;
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init();
    }

    protected function arrangeTrait(): object
    {
        $trait = new HasCrudObject();
        return $trait;
    }
}