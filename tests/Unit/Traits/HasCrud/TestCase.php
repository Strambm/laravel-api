<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use PHPUnit\Framework\TestCase as ParentTestCase;
use Cetria\Laravel\Api\Traits\HasCrud;

class TestCase extends ParentTestCase
{
    protected function arrangeTrait(string $modelClass): object
    {
        $trait = new class()
        {
            use HasCrud;

            public static $_modelClass;

            public static function getModelClass(): string
            {
                return self::$_modelClass;
            }
        };
        $trait::$_modelClass = $modelClass;
        return $trait;
    }
}