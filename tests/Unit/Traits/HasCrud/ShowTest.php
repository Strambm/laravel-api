<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Traits\HasCrud;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;

class ShowTest extends TestCase
{
    #[Test]
    public function error404(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $trait = $this->arrangeTrait(Product::class);
        $trait->show(new Request(), 1);
    }

    #[Test]
    public function correct(): void
    {
        $product = Product::factory()->create();
        $trait = $this->arrangeTrait(Product::class);
        $result = $trait->show(new Request(), $product->getKey());
        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
        $content = json_decode($result->getContent(), true);
        $this->assertEquals($product->id, $content['id']);
    }

    /**
     * @return CrudControllerInterface
     */
    protected function arrangeTrait(string $modelClass)
    {
        /** @var MockObject|CrudControllerInterface $trait */
        $trait = $this->getMockBuilder(HasCrudObject::class)
            ->onlyMethods(['_getModelClass'])
            ->getMock();
        $trait->expects($this->any())
            ->method('_getModelClass')
            ->willReturn($modelClass);
        return $trait;
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init(); 
    }
}