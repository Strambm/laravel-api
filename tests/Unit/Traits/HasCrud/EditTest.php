<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Cetria\Laravel\Form\Method;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;
use Cetria\Laravel\Api\Tests\Dummy\Product as ToFormProduct;
use Cetria\Laravel\Helpers\Test\Dummy\Product as DummyProduct;

class EditTest extends TestCase
{
    private $actionUrl = 'www.nextaction.com';

    #[Test]
    public function notImplemented(): void
    {
        $model = DummyProduct::factory()->create();
        $trait = $this->arrangeTrait(DummyProduct::class);
        $result = $trait->edit(new Request(), $model->getKey());
        $this->assertEquals(Response::HTTP_NOT_IMPLEMENTED, $result->getStatusCode());
    }

    #[Test]
    public function toFormInstance(): void
    {
        $model = ToFormProduct::factory()->create();
        $trait = $this->arrangeTrait(ToFormProduct::class);
        $result = $trait->edit(new Request(), $model->getKey());
        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
        $content = json_decode($result->getContent(), true)['message'];
        $this->assertEquals($this->actionUrl, $content['params']['action']);
        $this->assertEquals('POST', $content['params']['method']);
        $this->assertArrayHasKey('_method', $content['content']);
        $this->assertEquals(Method::PUT->name, $content['content']['_method']['value']);
    }

    /**
     * @return CrudControllerInterface
     */
    protected function arrangeTrait(string $testModelClass)
    {
        /** @var MockObject|CrudControllerInterface $trait */
        $trait = $this->getMockBuilder(HasCrudObject::class)
            ->onlyMethods(['_getModelClass', 'editVerify', 'getNextActionUrl'])
            ->getMock();
        $trait->expects($this->any())
            ->method('_getModelClass')
            ->willReturn($testModelClass);
        $trait->expects($this->atLeastOnce())
            ->method('editVerify');
        $trait->expects($this->any())
            ->method('getNextActionUrl')
            ->willReturn($this->actionUrl);
        return $trait;
    }
}