<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Mockery;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Factory as ValidationFactory;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;
use Illuminate\Foundation\Providers\FoundationServiceProvider;
use Illuminate\Contracts\Validation\Factory as ValidationFactoryContract;

class UpdateValidateTest extends TestCase
{
    #[Test]
    public function emptyValidator(): void
    {
        $trait = $this->arrangeTrait([]);
        $result = $trait->updateValidate(new Request());
        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
    }

    #[Test]
    public function correct(): void
    {
        $trait = $this->arrangeTrait([
            'name' => 'max:255|min:3',
            'value' => 'numeric|between:-5,5',
        ]);
        $result = $trait->updateValidate(new Request([
            'value' => 0,
        ]));
        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
    }

    #[Test]
    public function incorrect(): void
    {
        $trait = $this->arrangeTrait([
            'value' => 'numeric|between:-5,5',
        ]);
        $this->expectException(ValidationException::class);
        $trait->updateValidate(new Request([
            'name' => 'name',
            'value' => 6,
        ]));
    }

    /**
     * @return CrudControllerInterface
     * 
     */
    protected function arrangeTrait(array $validatorRules)
    {
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudObject::class)
            ->onlyMethods(['updateValidateRules'])
            ->getMock();
        $trait->expects($this->any())
            ->method('updateValidateRules')
            ->willReturn($validatorRules);
        return $trait;
    }

    protected function setUp(): void
    {
        parent::setUp();
        $initValidatorInRequest = new FoundationServiceProvider(app());
        $initValidatorInRequest->registerRequestValidation();
        Auth::shouldReceive('check')
            ->once()
            ->andReturn(true);
        app()->singleton('translator', function ($app) {
            return new \Illuminate\Translation\Translator(new \Illuminate\Translation\ArrayLoader(), 'en');
        });
        app()->singleton(ValidationFactoryContract::class, function ($app) {
            return new ValidationFactory($app['translator'], $app);
        });

        app()->alias(ValidationFactoryContract::class, 'validator');
    }

    public function tearDown(): void
{
    Mockery::close();
    parent::tearDown();
}
}