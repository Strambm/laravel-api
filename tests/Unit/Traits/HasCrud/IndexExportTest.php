<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Mockery;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Bus;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Facade;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Api\Tasks\MakeExport;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tests\Dummy\Product;
use Cetria\Laravel\Api\Tasks\SendMailParams;
use Cetria\Laravel\Api\Models\ExportDrivers\XML;
use Illuminate\Contracts\Bus\Dispatcher as BusDispatcher;

class IndexExportTest extends TestCase
{
    protected $url = 'www.tmp.cz';
    protected $app;
    protected $compareJobMethod;

    #[Test]
    public function pingToUrl(): void
    {
        $trait = $this->arrangeTrait(Product::class);
        $request = $this->arrangeRequest(['urlToPing' => $this->url, 'format' => 'xml']);
        $this->compareJobMethod = function(MakeExport $job): bool {
            $params = Reflection::getHiddenProperty($job, 'params');
            return Reflection::getHiddenProperty($params, 'returnUrl') == $this->url
                && Reflection::getHiddenProperty($params, 'driver') == XML::class;
            return true;
        };
        $result = $trait->indexExport($request);
        $this->assertEquals(Response::HTTP_ACCEPTED, $result->getStatusCode());
    }

    #[Test]
    public function sendEmailToExport(): void
    {
        $trait = $this->arrangeTrait(Product::class);
        $request = $this->arrangeRequest(['email' => 'test@test.cz', 'format' => 'xml']);
        $this->compareJobMethod = function(MakeExport $job): bool {
            $params = Reflection::getHiddenProperty($job, 'params');
            $mailParams = Reflection::getHiddenProperty($params, 'returnEmailParams');
            return Reflection::getHiddenProperty($params, 'returnUrl') == null
                && $mailParams instanceof SendMailParams
                && $mailParams->getEmail() == 'test@test.cz'
                && Reflection::getHiddenProperty($params, 'driver') == XML::class;
        };
        $result = $trait->indexExport($request);
        $this->assertEquals(Response::HTTP_ACCEPTED, $result->getStatusCode());
    }

    protected function arrangeRequest(mixed ...$params): Request
    {
        $class = new class(... $params) extends Request
        {
            public function validate(array $filter): void
            {

            }
        };
        return $class;
    }

    protected function arrangeTrait(string $modelClass, int $itemsOnPage = 100): HasCrudObject
    {
        /** @var MockObject|HasCrudObject $trait */
        $trait = $this->getMockBuilder(HasCrudObject::class)
            ->onlyMethods(['_getModelClass'])
            ->getMock();
        $trait->expects($this->any())
            ->method('_getModelClass')
            ->willReturn($modelClass);
        Reflection::setHiddenProperty($trait, 'itemsOnPage', $itemsOnPage);
        return $trait;
    }

    protected function setUp(): void
    {
        parent::setUp();
        $busMock = $this->getMockBuilder(BusDispatcher::class)
            ->disableOriginalConstructor()
            ->getMock();
        $busMock->expects($this->atLeastOnce())->method('dispatch')->with($this->callback(function ($job): bool {
            if (!$job instanceof MakeExport) {
                return false;
            }
            $method = $this->compareJobMethod;
            return $method($job);
        }));
        $container = Container::getInstance();
        $container->singleton(BusDispatcher::class, function () use ($busMock) {
            return $busMock;
        });
        $container->instance(Application::class, $app = Mockery::mock(Application::class));
        Facade::setFacadeApplication($container);
        Bus::swap($busMock);
        $this->app = $container;
    }
}