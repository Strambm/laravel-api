<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Laravel\Api\Tests\Dummy\User;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tests\Dummy\Order;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;

class FillModelRelatedModelsTest extends TestCase
{
    #[Test]
    public function relatedModelExists(): void
    {
        $user = User::factory()->create();
        $trait = $this->arrangeTrait();
        $sourceModel = $this->arrangeSourceModel(['user' => User::class]);
        $this->act(
            $trait, 
            $sourceModel, 
            new Request([
                'user.name' => $user->name,
            ])
        );
        $this->assertEquals($user->id, $sourceModel->user_id);
    }

    #[Test]
    public function relatedModelUnexists(): void
    {
        $userName = Str::random(8);
        $trait = $this->arrangeTrait();
        $sourceModel = $this->arrangeSourceModel(['user' => User::class]);
        $this->act(
            $trait, 
            $sourceModel, 
            new Request([
                'user.name' => $userName,
            ])
        );
        /** @var null|User $storedUser */
        $storedUser = User::where('name', $userName)->first();
        $this->assertNotNull($storedUser);
        $this->assertEquals($storedUser->getKey(), $sourceModel->user_id);
    }

    protected function arrangeSourceModel(array $getToFormRelatedModelsResult): Model
    {
        $model = new Order();
        Reflection::setHiddenProperty($model, 'toFormRelatedModels', $getToFormRelatedModelsResult);
        return $model;
    }

    protected function arrangeTrait(): object
    {
        $trait = new HasCrudObject();
        return $trait;
    }

    protected function act($trait, Model &$model, Request $request): void
    {
        $methodName = 'fillModelRelatedModels';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        $method->invokeArgs($trait, [&$model, $request]);
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init(); 
    }
}