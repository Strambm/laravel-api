<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\Attributes\DataProvider;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;
use Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\HasCrudObject;

class UpdateValidateRulesTest extends TestCase
{
    #[Test]
    #[DataProvider('basicDataProviderMethod')]
    public function basic(array $storeRules, array $expectedUpdateRules): void
    {
        $result = $this->act($storeRules);
        $this->assertEquals($expectedUpdateRules, $result);
    }

    public static function basicDataProviderMethod(): array
    {
        return [
            [
                [
                    'name' => 'required'
                ], [

                ]
            ], [
                [
                    'name' => ['required']
                ], [

                ]
            ], [
                [
                    'name' => 'required|min:3'
                ], [
                    'name' => ['min:3']
                ]
            ], [
                [
                    'name' => 'max:3'
                ], [
                    'name' => ['max:3']
                ]
            ] 
        ];
    }

    protected function act(array $storeRules): array
    {
        /** @var MockObject|CrudControllerInterface $trait */
        $trait = $this->getMockBuilder(HasCrudObject::class)
            ->onlyMethods(['storeValidateRules'])
            ->getMock();
        $trait->expects($this->any())
            ->method('storeValidateRules')
            ->willReturn($storeRules);
        $methodName = 'updateValidateRules';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        return $method->invoke($trait);
    }
}