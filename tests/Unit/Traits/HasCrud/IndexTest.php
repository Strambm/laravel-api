<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Traits\HasCrud;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tests\Dummy\Product;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;

class IndexTest extends TestCase
{
    #[Test]
    public function basic(): void
    {
        $count = 5;
        $itemOnPage = 2;
        Product::
            factory()
            ->count($count)
            ->create();
        $trait = $this->arrangeTrait(Product::class, $itemOnPage);
        $result = $trait->index(new Request());
        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
        $content = json_decode($result->getContent(), true);
        $this->assertEquals($count, $content['header']['itemCount']);
        $this->assertEquals(0, $content['header']['offset']);
        $this->assertCount($itemOnPage, $content['message']);
    }

    #[Test]
    public function offset(): void
    {
        $count = 5;
        $itemOnPage = 2;
        $offset = 4;
        Product::
            factory()
            ->count($count)
            ->create();
        $trait = $this->arrangeTrait(Product::class, $itemOnPage);
        $result = $trait->index(new Request(['offset' => $offset]));
        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
        $content = json_decode($result->getContent(), true);
        $this->assertEquals($count, $content['header']['itemCount']);
        $this->assertEquals($offset, $content['header']['offset']);
        $this->assertCount(1, $content['message']);
    }

    #[Test]
    public function limit(): void
    {
        $count = 2;
        $itemOnPage = 100;
        $limit = 1;
        Product::
            factory()
            ->count($count)
            ->create();
        $trait = $this->arrangeTrait(Product::class, $itemOnPage);
        $result = $trait->index(new Request(['limit' => $limit]));
        $this->assertEquals(Response::HTTP_OK, $result->getStatusCode());
        $content = json_decode($result->getContent(), true);
        $this->assertEquals($count, $content['header']['itemCount']);
        $this->assertEquals(0, $content['header']['offset']);
        $this->assertCount($limit, $content['message']);
    }

    /**
     * @return CrudControllerInterface
     */
    protected function arrangeTrait(string $modelClass, int $itemsOnPage = 100)
    {
        /** @var MockObject|CrudControllerInterface $trait */
        $trait = $this->getMockBuilder(HasCrudObject::class)
            ->onlyMethods(['_getModelClass'])
            ->getMock();
        $trait->expects($this->any())
            ->method('_getModelClass')
            ->willReturn($modelClass);
        Reflection::setHiddenProperty($trait, 'itemsOnPage', $itemsOnPage);
        return $trait;
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init(); 
    }
}