<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Illuminate\Http\Request;
use Cetria\Laravel\Form\Method;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Filter;
use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Builder;
use Cetria\Laravel\Api\Tests\Dummy\Product;
use PHPUnit\Framework\Attributes\DataProvider;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Illuminate\Database\Eloquent\Relations\Relation;
use PHPUnit\Framework\Attributes\DoesNotPerformAssertions;
use Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\HasCrudObject;

class TryApplyFilterTest extends TestCase
{   
    #[Test]
    #[DataProvider('applyFilterDataProvider')]
    public function applyFilter(Builder|Relation $query): void
    {
        $column = 'testColumn';
        $operator = Operator::EQ;
        $value = 'test';
        $filterString = (new Filter())
            ->addScope(new Condition($column, $operator, $value))
            ->__toString();
        $request = Request::create('/your-route', Method::GET->name, ['filter' => $filterString]);
        $result = $this->act($query, $request);
        $wheres = $this->getWheres($result);
        $where = $wheres[0]['query']->wheres[0];
        $this->assertEquals($column, $where['column']);
        $this->assertEquals($operator->value, $where['operator']);
        $this->assertEquals($value, $where['value']);
    }

    #[Test]
    #[DataProvider('applyFilterDataProvider')]
    #[DoesNotPerformAssertions]
    public function withoutFilter(Builder|Relation $query): void
    {
        $request = Request::create('/your-route', Method::GET->name, ['filter' => null]);
        $this->act($query, $request);

        $request = Request::create('/your-route', Method::GET->name, ['filter' => '']);
        $this->act($query, $request);
    }

    public static function applyFilterDataProvider(): array
    {
        DummyHelper::init();
        return [
            [
                Product::query()
            ], [
                (new Product())->basketItems()
            ]
        ];
    }

    private function act(Builder|Relation $query, Request $request): Builder|Relation
    {
        $hasCrud = new HasCrudObject();
        $methodName = 'tryApplyFilter';
        $method = Reflection::getHiddenMethod($hasCrud, $methodName);
        return $method->invoke($hasCrud, $query, $request);
    }

    private function getWheres(Builder|Relation $query): array
    {
        if($query instanceof Builder) {
            $wheres = $query->getQuery()->wheres;
        } else {
            $wheres = $query->getQuery()->getQuery()->wheres;
            $wheres = array_values(array_filter($wheres, function(array $where): bool {
                return $where['type'] == 'Nested';
            }));
            $wheres = $wheres[0]['query']->wheres;
        }
        return $wheres;
    }
}