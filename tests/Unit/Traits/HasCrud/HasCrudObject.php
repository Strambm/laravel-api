<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud;

use Cetria\Laravel\Api\Traits\HasCrud;
use Cetria\Laravel\Api\Interfaces\EnableSendEmailsInterface;

class HasCrudObject
implements EnableSendEmailsInterface
{
    use HasCrud;

    public static $getModelClassResult = '';

    public static function getModelClass(): string
    {
        return self::$getModelClassResult;
    }

    public function getEmailClass(): string
    {
        return static::class;
    }
}