<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\HasCrudViaPivotObject;

class GetPivotAttributesFromRequestTest extends TestCase
{
    #[Test]
    public function basic(): void
    {
        $request = new Request([
            'pivot.name' => 1,
            'name' => 2,
            'pivot.test.name' => 3,
        ]);
        $result = $this->act($request, 'pivot.');
        $this->assertEquals(['name' => 1], $result);
    }

    protected function act(Request $request, string $prefix): array
    {
        $trait = new HasCrudViaPivotObject();
        $methodName = 'getPivotAttributesFromRequest';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        return $method->invoke($trait, $request, $prefix);
    }
}