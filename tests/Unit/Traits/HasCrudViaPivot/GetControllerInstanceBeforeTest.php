<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot;

use PHPUnit\Framework\TestCase;
use Illuminate\Routing\Controller;
use Cetria\Laravel\Api\Traits\HasCrud;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;
use Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\HasCrudViaPivotObject;

class GetControllerInstanceBeforeTest extends TestCase
{
    #[Test]
    public function test(): void
    {
        $constructorBefore = new class() extends Controller implements CrudControllerInterface
        {
            use HasCrud;

            public static function getModelClass(): string
            {
                return '';
            }
        };
        /** @var MockObject|CrudControllerInterface $trait */
        $trait = $this->getMockBuilder(HasCrudViaPivotObject::class)
            ->onlyMethods(['getControllerClassBefore'])
            ->getMock();
        $trait->expects($this->any())
            ->method('getControllerClassBefore')
            ->willReturn(get_class($constructorBefore));
        $result = $this->act($trait);
        $this->assertEquals(get_class($constructorBefore), get_class($result));
    }

    protected function act($trait)
    {
        $methodName = 'getControllerInstanceBefore';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        return $method->invoke($trait);
    }
}