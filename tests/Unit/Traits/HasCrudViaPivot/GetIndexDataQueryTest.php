<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Filter;
use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Tests\Dummy\Product;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\Category;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;

class GetIndexDataQueryTest extends TestCase
{
    #[Test]
    public function basic(): void
    {
        $modelBefore = Product::factory()
            ->create();
        $modelBefore = Product::findOrFail($modelBefore->getKey());
        $relationName = 'categories';
        $models = Category::factory()
            ->count(3)
            ->create();
        $modelBefore->$relationName()->saveMany($models);
        $trait = $this->arrangeTrait($modelBefore, $relationName);
        $result = $this->act($trait, new Request(), [])->get();
        $this->assertCount(3, $result);
        $sourceCompareModel = $models->first();
        $resultCompareModel = $result->where($sourceCompareModel->getKeyName(), $sourceCompareModel->getKey())->first();
        $this->assertEquals($sourceCompareModel->getAttributes(), $resultCompareModel->getAttributes());
    }

    #[Test]
    public function withFilterQuery(): void
    {
        $modelBefore = Product::factory()
            ->create();
        $modelBefore = Product::findOrFail($modelBefore->getKey());
        $relationName = 'categories';
        $models = Category::factory()
            ->count(3)
            ->create();
        $modelBefore->$relationName()->saveMany($models);
        $modelBefore->$relationName()->save(Category::factory()->create(['name' => 'unfltered']));
        $trait = $this->arrangeTrait($modelBefore, $relationName);
        $result = $this->act(
            $trait, 
            new Request(['filter' => (new Filter())->addScope(new Condition('name', Operator::NEQ, 'unfltered'))->__toString()]), 
            []
        )->get();
        $this->assertCount(3, $result);
        $sourceCompareModel = $models->first();
        $resultCompareModel = $result->where($sourceCompareModel->getKeyName(), $sourceCompareModel->getKey())->first();
        $this->assertEquals($sourceCompareModel->getAttributes(), $resultCompareModel->getAttributes());
    }

    protected function arrangeTrait(Model $modelBefore, string $relationName)
    {
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudViaPivotObject::class)
            ->onlyMethods(['getModelBefore', 'getRelationNameFromModelBefore'])
            ->getMock();
        $trait->expects($this->any())
            ->method('getModelBefore')
            ->willReturn($modelBefore);
        $trait->expects($this->any())
            ->method('getRelationNameFromModelBefore')
            ->willReturn($relationName);
        return $trait;
    }

    protected function act($trait, Request $request, array $ids)
    {
        $methodName = 'getIndexDataQuery';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        return $method->invoke($trait, $request, $ids);
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init();
    }
}