<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Api\Traits\HasCrudViaPivot;
use Cetria\Laravel\Helpers\Test\Dummy\Category;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;

class GetModelInstanceTest extends TestCase
{
    #[Test]
    public function withId(): void
    {
        $category = Category::factory()
            ->create();
        $noiseCategory = Category::factory()
            ->create();
        $trait = $this->arrangeTrait();
        $result = $this->act($trait, new Request([
            $category->getKeyName() => $category->getKey(),
            'name' => $noiseCategory->name,
        ]));
        $this->assertEquals($category->getAttributes(), $result->getAttributes());
    }

    #[Test]
    public function withoutId(): void
    {
        $category = Category::factory()
            ->create();
        $trait = $this->arrangeTrait();
        $result = $this->act($trait, new Request([
            'name' => $category->name,
        ]));   
        $this->assertEquals($category->getAttributes(), $result->getAttributes());
    }

    #[Test]
    public function unexist(): void
    {
        $trait = $this->arrangeTrait();
        $result = $this->act($trait, new Request([
            'name' => 'unexistName',
        ]));  
        $this->assertEquals($result->name, 'unexistName');
        $this->assertFalse($result->exists);
    }

    protected function arrangeTrait()
    {
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudViaPivotObject::class)
            ->onlyMethods(['_getModelClass'])
            ->getMock();
        $trait->expects($this->any())
            ->method('_getModelClass')
            ->willReturn(Category::class);
        return $trait;
    }

    protected function act($trait, Request $request)
    {
        $methodName = 'getModelInstance';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        return $method->invoke($trait, $request);
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init();
    }
}