<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Illuminate\Routing\Controller;
use Cetria\Laravel\Api\Traits\HasCrud;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;

class GetModelBeforeTest extends TestCase
{
    #[Test]
    public function test(): void
    {
        $model = Product::factory()->make();
        $trait = $this->arrangeTrait($model);
        $result = $this->act($trait, [52, 72, 44]);
        $this->assertEquals($model, $result);
    }

    protected function arrangeTrait(Model $expectedModel)
    {
        $constructorBefore = new class($expectedModel) extends Controller implements CrudControllerInterface
        {
            use HasCrud;

            private $expectedModel;
            
            public function __construct(Model $expectedModel)
            {
                $this->expectedModel = $expectedModel;
            }

            public static function getModelClass(): string
            {
                return '';
            }

            protected function getShowData(Request $request, array $ids): Model
            {
                if($ids[0] == 52 && $ids[1] == 72) {
                    return $this->expectedModel;
                }
            }
        };
        /** @var MockObject|CrudControllerInterface $trait */
        $trait = $this->getMockBuilder(HasCrudViaPivotObject::class)
            ->onlyMethods(['getControllerInstanceBefore'])
            ->getMock();
        $trait->expects($this->atLeastOnce())
            ->method('getControllerInstanceBefore')
            ->willReturn($constructorBefore);
        return $trait;
    }

    protected function act($trait, mixed ...$params): Model
    {
        $methodName = 'getModelBefore';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        return $method->invoke($trait, ...$params);
    }
}