<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot;

use Cetria\Laravel\Api\Traits\HasCrudViaPivot;

class HasCrudViaPivotObject
{
    use HasCrudViaPivot;

    protected function getControllerClassBefore(): string
    {
        '';
    }

    protected function getRelationNameFromModelBefore(): string
    {
        return '';
    }

    public static function getModelClass(): string
    {
        return '';
    }
}