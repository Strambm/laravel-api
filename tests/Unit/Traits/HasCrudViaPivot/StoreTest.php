<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Helpers\Test\Dummy\Category;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;
use Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\HasCrudViaPivotObject;

class StoreTest extends TestCase
{
    #[Test]
    public function transactionFailed(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('test');
        $trait = $this->arrangeTraitForTransactionFailed();
        try{
            $response = $trait->store(new Request(['name' => 'testName']));
        } catch(Exception $e) {
            $this->assertEquals(0, $this->getPivotModelsQuery()->count());
            $this->assertEquals(0, Category::query()->count());
            throw $e;
        }
    }

    /**
     * @return CrudControllerInterface
     */
    protected function arrangeTraitForTransactionFailed()
    {
        $related = Product::factory()
            ->create();
        $relation = $this->getMockBuilder(BelongsToMany::class)
            ->onlyMethods(['save'])
            ->setConstructorArgs([
                (new Category())->newQuery(),
                $related,
                'category_product',
                'product_id',
                'category_id',
                'id',
                'id',
                'categories'
            ])
            ->getMock();
        $relation->expects($this->any())
            ->method('save')
            ->willThrowException(new Exception('test'));
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudViaPivotObject::class)
            ->onlyMethods(['getRelation', 'storeVerify', '_getModelClass'])
            ->getMock();
        $trait->expects($this->any())
            ->method('getRelation')
            ->willReturn($relation);
        $trait->expects($this->any())
            ->method('_getModelClass')
            ->willReturn(Category::class);
        return $trait;
    }

    #[Test]
    public function basic(): void
    {
        $modelBefore = Product::factory()
            ->create();
        $model = Category::factory()
            ->create();
        $trait = $this->arrangeTrait($modelBefore);
        $result = $trait->store(new Request([$model->getKeyName() => $model->getKey()]), $modelBefore->getKey());
        $this->assertEquals(Response::HTTP_CREATED, $result->getStatusCode());
        $this->assertEquals(1, $this->getPivotModelsQuery()->count());
        $pivot = $this->getPivotModelsQuery()->first();
        $this->assertEquals($modelBefore->getKey(), $pivot->product_id);
        $this->assertEquals($model->getKey(), $pivot->category_id);
        
    }

    #[Test]
    public function withCreateRelated(): void
    {
        $modelBefore = Product::factory()
            ->create();
        $model = Category::factory()
            ->make();
        $trait = $this->arrangeTrait($modelBefore);
        $result = $trait->store(new Request($model->getAttributes()), $modelBefore->getKey());
        $this->assertEquals(Response::HTTP_CREATED, $result->getStatusCode());
        $pivot = $this->getPivotModelsQuery()->first();
        $this->assertEquals($modelBefore->getKey(), $pivot->product_id);
        $this->assertEquals(1, Category::query()->count());
    }

    /**
     * @return CrudControllerInterface
     */
    protected function arrangeTrait(Model $modelBefore)
    {
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudViaPivotObject::class)
            ->onlyMethods(['getModelBefore', 'getRelationNameFromModelBefore', '_getModelClass', 'storeVerify'])
            ->getMock();
        $trait->expects($this->any())
            ->method('getModelBefore')
            ->willReturn($modelBefore);
        $trait->expects($this->any())
            ->method('getRelationNameFromModelBefore')
            ->willReturn('categories');
        $trait->expects($this->any())
            ->method('_getModelClass')
            ->willReturn(Category::class);
        return $trait;
    }

    protected function getPivotModelsQuery(): Builder
    {
        $pivotClass = new class() extends Model
        {
            protected $table = 'category_product';
        };
        /** @var Builder $query */
        $query = $pivotClass::query();
        return $query;
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init(); 
    }
}