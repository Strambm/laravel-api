<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Schema\Blueprint;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Helpers\Test\Dummy\Category;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Cetria\Laravel\Api\Tests\Dummy\ProductForUpdatePivot;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;
use Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\HasCrudViaPivotObject;

class UpdateTest extends TestCase
{
    #[Test]
    public function basic(): void
    {
        $modelBefore = Product::factory()
            ->create();
        $modelBefore = ProductForUpdatePivot::findOrFail($modelBefore->id);
        $model = Category::factory()
            ->create();
        $changedValue = 42;
        $modelBefore->categories()->save($model, ['tmp' => 1]);
        $trat = $this->arrangeTrait($modelBefore);
        $resut = $trat->update(new Request(['pivot.tmp' => $changedValue]), $modelBefore->getKey(), $model->getKey());
        $this->assertEquals(Response::HTTP_OK, $resut->getStatusCode());
        $this->assertEquals($changedValue, $this->getPivotModelsQuery()->first()->tmp);
        $content = json_decode($resut->getContent(), true)['message'];
        $this->assertEquals($model->id, $content['id']);
        $this->assertEquals($model->name, $content['name']);
        $this->assertEquals($changedValue, $content['pivot']['tmp']);
    }

    /**
     * @return CrudControllerInterface
     */
    protected function arrangeTrait(Model $modelBefore)
    {
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudViaPivotObject::class)
            ->onlyMethods(['getModelBefore', 'getRelationNameFromModelBefore', '_getModelClass', 'updateVerify'])
            ->getMock();
        $trait->expects($this->any())
            ->method('getModelBefore')
            ->willReturn($modelBefore);
        $trait->expects($this->any())
            ->method('getRelationNameFromModelBefore')
            ->willReturn('categories');
        $trait->expects($this->any())
            ->method('_getModelClass')
            ->willReturn(Category::class);
        return $trait;
    }

    protected function getPivotModelsQuery(): Builder
    {
        $pivotClass = new class() extends Model
        {
            protected $table = 'category_product';
        };
        /** @var Builder $query */
        $query = $pivotClass::query();
        return $query;
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init(); 
        $builder = Model::getConnectionResolver()->connection()->getSchemaBuilder();
        $builder->drop(
            'category_product'/*,
            function(Blueprint $table): void
            {
                $table->integer('tmp');
            }*/
        );
        $builder->create(
            'category_product',
            function(Blueprint $table): void
            {
                $table->integer('product_id');
                $table->integer('category_id');
                $table->integer('tmp');
            }
        );
    }
}