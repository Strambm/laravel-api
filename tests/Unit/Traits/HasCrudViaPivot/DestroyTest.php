<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Helpers\Test\Dummy\Category;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DestroyTest extends TestCase
{
    #[Test]
    public function error404(): void
    {
        $modelBefore = Product::factory()
            ->create();
        $model = Category::factory()
            ->create();
        $trait = $this->arrangeTrait($modelBefore);
        $this->expectException(ModelNotFoundException::class);
        $trait->destroy(new Request(), $modelBefore->getKey(), $model->getKey());
    }

    #[Test]
    public function correct(): void
    {
        $modelBefore = Product::factory()
            ->create();
        $model = Category::factory()
            ->create();
        $modelBefore->categories()->save($model);
        $trait = $this->arrangeTrait($modelBefore);
        $result = $trait->destroy(new Request(), $modelBefore->getKey(), $model->getKey());
        $this->assertEquals(Response::HTTP_NO_CONTENT, $result->getStatusCode());
        $this->assertEquals(0, $this->getPivotModelsQuery()->count());
    }

    /**
     * @return CrudControllerInterface
     */
    protected function arrangeTrait(Model $modelBefore)
    {
        /** @var MockObject $trait */
        $trait = $this->getMockBuilder(HasCrudViaPivotObject::class)
            ->onlyMethods(['getModelBefore', 'getRelationNameFromModelBefore', '_getModelClass', 'destroyVerify'])
            ->getMock();
        $trait->expects($this->any())
            ->method('getModelBefore')
            ->willReturn($modelBefore);
        $trait->expects($this->any())
            ->method('getRelationNameFromModelBefore')
            ->willReturn('categories');
        $trait->expects($this->any())
            ->method('_getModelClass')
            ->willReturn(Category::class);
        return $trait;
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init(); 
    }

    protected function getPivotModelsQuery(): Builder
    {
        $pivotClass = new class() extends Model
        {
            protected $table = 'category_product';
        };
        /** @var Builder $query */
        $query = $pivotClass::query();
        return $query;
    }
}