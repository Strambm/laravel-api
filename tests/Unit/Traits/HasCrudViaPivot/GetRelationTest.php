<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot;

use PHPUnit\Framework\Attributes\DataProvider;
use TypeError;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Test\Dummy\Order;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class GetRelationTest extends TestCase
{
    #[Test]
    public function belongsToMany(): void
    {
        $trait = $this->arrangeTrait('categories', Product::class);
        $result = $this->act($trait);
        $this->assertInstanceOf(BelongsToMany::class, $result);
    }

    #[Test]
    #[DataProvider('badRelationTypeDataProviderMethod')]
    public function badRelationType(string $relationName, string $modelClass): void
    {
        $this->expectException(TypeError::class);
        $trait = $this->arrangeTrait($relationName, $modelClass);
        $this->act($trait);
    }

    public static function badRelationTypeDataProviderMethod(): array
    {
        return [
            [ //HasMany
                'basketItems',
                Product::class
            ], [ //BelongsTo
                'user',
                Order::class
            ]
        ];
    }

    protected function arrangeTrait(string $relationName, string $modelClass)
    {
        $model = $modelClass::factory()->create();
        /** @var MockObject $trat */
        $trait = $this->getMockBuilder(HasCrudViaPivotObject::class)
            ->onlyMethods(['getRelationNameFromModelBefore', 'getModelBefore'])
            ->getMock();
        $trait->expects($this->any())
            ->method('getRelationNameFromModelBefore')
            ->willReturn($relationName);
        $trait->expects($this->any())
            ->method('getModelBefore')
            ->willReturn($model);
        return $trait;
    }

    protected function act($trait): BelongsToMany
    {
        $methodName = 'getRelation';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        return $method->invoke($trait, [1]);
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init();
    }
}