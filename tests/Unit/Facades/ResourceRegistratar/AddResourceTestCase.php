<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Facades\ResourceRegistratar;

use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use PHPUnit\Framework\TestCase;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Illuminate\Routing\RouteCollection;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Facades\ResourceRegistrar;

abstract class AddResourceTestCase extends TestCase
{
    protected $name = 'testName';
    protected $controller = 'TestingController';

    protected function arrangeResourceRegistratar(): ResourceRegistrar
    {
        $registratar = new ResourceRegistrar($this->arrangeRouter());
        return $registratar;
    }

    protected function arrangeRouter(): Router
    {
        $mockEvents = $this->createMock(Dispatcher::class);
        $mockContainer = $this->createMock(Container::class);
        $router = new Router($mockEvents, $mockContainer);
        return $router;
    }

    protected function act(ResourceRegistrar $registratar): Route
    {
        $methodName = $this->getTestMethodName();
        $method = Reflection::getHiddenMethod($registratar, $methodName);
        $route = $method->invoke($registratar, $this->name, '/test', $this->controller, []);
        return $route;
    }

    protected abstract function getTestMethodName(): string;

    protected function assertRouteWasDefined(Route $result, string $postfix): void
    {
        /** @var Router $router */
        $router = Reflection::getHiddenProperty($result, 'router');
        /** @var RouteCollection $routes */
        $routes = $router->getRoutes();
        $routesParam = Reflection::getHiddenProperty($routes, 'routes');
        $this->assertArrayHasKey('POST', $routesParam);
        $this->assertArrayHasKey($this->name . '/' . $postfix, $routesParam['POST']);

        $nameListParam = Reflection::getHiddenProperty($routes, 'nameList');
        $this->assertArrayHasKey($this->name . '.' . $postfix, $nameListParam);

        $actionListParam = Reflection::getHiddenProperty($routes, 'actionList');
        $this->assertArrayHasKey($this->controller . '@' . $postfix, $actionListParam);
    }
}