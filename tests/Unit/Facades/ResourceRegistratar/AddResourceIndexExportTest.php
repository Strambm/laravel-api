<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Facades\ResourceRegistratar;
use PHPUnit\Framework\Attributes\Test;

class AddResourceIndexExportTest extends AddResourceTestCase
{
    #[Test]
    public function testMethod(): void
    {
        $registratar = $this->arrangeResourceRegistratar();
        $route = $this->act($registratar);
        $this->assertRouteWasDefined($route, 'indexExport');
    }

    protected function getTestMethodName(): string
    {
        return 'addResourceIndexExport';
    }
}