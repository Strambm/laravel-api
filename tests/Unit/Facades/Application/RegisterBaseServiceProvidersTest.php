<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Facades\Application;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Illuminate\Log\LogServiceProvider;
use Cetria\Laravel\Api\Facades\Application;
use Illuminate\Events\EventServiceProvider;
use Cetria\Laravel\Api\Providers\CustomRouteServiceProvider;

class RegisterBaseServiceProvidersTest extends TestCase
{
    #[Test]
    public function baseServiceProvidersAreRegistered(): void
    {
        $app = $this->createApplication();
        $providers = $app->getLoadedProviders();

        $this->assertArrayHasKey(EventServiceProvider::class, $providers);
        $this->assertArrayHasKey(LogServiceProvider::class, $providers);
        $this->assertArrayHasKey(CustomRouteServiceProvider::class, $providers);
    }
    
    protected function createApplication(): Application
    {
        return new Application();
    }
}