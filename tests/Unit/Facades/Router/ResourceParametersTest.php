<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Facades\Router;

use Cetria\Laravel\Api\Facades\ResourceRegistrar;
use Cetria\Laravel\Api\Tests\Unit\Facades\Router\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\Test;

class ResourceParametersTest extends TestCase
{
    #[Test]
    public function resourceParametersMethodCallsSetParametersOnResourceRegistrar(): void
    {
        $parameters = ['param1' => 'value1', 'param2' => 'value2'];
        $this->assertEquals([], Reflection::getHiddenProperty(ResourceRegistrar::class, 'parameterMap'));
        $this->act($parameters);
        $this->assertEquals($parameters, Reflection::getHiddenProperty(ResourceRegistrar::class, 'parameterMap'));
    }

    protected function act(array $params): void
    {
        $router = $this->getRouter();
        $router->resourceParameters($params);
    }
}