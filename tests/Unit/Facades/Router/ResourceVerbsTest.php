<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Facades\Router;

use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Facades\ResourceRegistrar;
use Cetria\Laravel\Api\Tests\Unit\Facades\Router\TestCase;
use PHPUnit\Framework\Attributes\Test;

class ResourceVerbsTest extends TestCase
{
    protected $defaultVerbs;
    
    #[Test]
    public function resourceVerbsMethodCallsVerbsOnResourceRegistrar__add(): void
    {
        $verbs = ['test', 'ada'];
        Reflection::setHiddenProperty(ResourceRegistrar::class, 'verbs', []);
        $this->act($verbs);
        $this->assertEquals($verbs, Reflection::getHiddenProperty(ResourceRegistrar::class, 'verbs'));
    }

    #[Test]
    public function resourceVerbsMethodCallsVerbsOnResourceRegistrar__setEmpty(): void
    {
        $verbs = ['test', 'ada'];
        Reflection::setHiddenProperty(ResourceRegistrar::class, 'verbs', $verbs);
        $this->act([]);
        $this->assertEquals($verbs, Reflection::getHiddenProperty(ResourceRegistrar::class, 'verbs'));
    }

    protected function act(array $verbs): void
    {
        $router = $this->getRouter();
        $router->resourceVerbs($verbs);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->defaultVerbs = Reflection::getHiddenProperty(ResourceRegistrar::class, 'verbs');
    }

    protected function tearDown(): void
    {
        Reflection::setHiddenProperty(ResourceRegistrar::class, 'verbs', $this->defaultVerbs);
        parent::tearDown();
    }
}