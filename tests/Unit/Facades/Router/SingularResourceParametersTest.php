<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Facades\Router;

use Cetria\Laravel\Api\Facades\ResourceRegistrar;
use Cetria\Laravel\Api\Tests\Unit\Facades\Router\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\Test;

class SingularResourceParametersTest extends TestCase
{
    #[Test]
    public function singularResourceParametersMethodCallsSetSingularParametersOnResourceRegistrar(): void
    {
        $singular = false;
        $this->assertEquals(!$singular, Reflection::getHiddenProperty(ResourceRegistrar::class, 'singularParameters'));
        $this->act($singular);
        $this->assertEquals($singular, Reflection::getHiddenProperty(ResourceRegistrar::class, 'singularParameters'));
    }

    protected function act(bool $singular): void
    {
        $router = $this->getRouter();
        $router->singularResourceParameters($singular);
    }
}