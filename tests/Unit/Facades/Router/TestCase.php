<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Facades\Router;

use PHPUnit\Framework\TestCase as BasicTestCase;
use Cetria\Laravel\Api\Facades\Router;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Facades\ResourceRegistrar;
use Illuminate\Routing\PendingResourceRegistration;
use Illuminate\Routing\PendingSingletonResourceRegistration;

class TestCase extends BasicTestCase
{
    protected function getRouter(): Router
    {
        $mockEvents = $this->createMock(Dispatcher::class);
        $mockContainer = $this->createMock(Container::class);
        $router = new Router($mockEvents, $mockContainer);
        return $router;
    }

    protected function assertCompareResourceRegistration(PendingResourceRegistration|PendingSingletonResourceRegistration $registration, string $expectedName, string $expectedControllerClass, array $expectedOptions): void
    {
        $this->assertEquals($expectedName, Reflection::getHiddenProperty($registration, 'name'));
        $this->assertEquals($expectedControllerClass, Reflection::getHiddenProperty($registration, 'controller'));
        $this->assertEquals($expectedOptions, Reflection::getHiddenProperty($registration, 'options'));
        $this->assertInstanceOf(ResourceRegistrar::class, Reflection::getHiddenProperty($registration, 'registrar'));
    }
}