<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Facades\Router;

use Cetria\Laravel\Api\Tests\Unit\Facades\Router\TestCase;
use Illuminate\Routing\PendingSingletonResourceRegistration;
use PHPUnit\Framework\Attributes\Test;

class SingletonTest extends TestCase
{
    #[Test]
    public function returnsPendingSingletonResourceRegistration(): void
    {
        $name = 'test';
        $controller = 'TestController';
        $options = ['foo' => 'bar'];

        $result = $this->act($name, $controller, $options);
        $this->assertCompareResourceRegistration($result, $name, $controller, $options);
    }

    protected function act(string $name, string $controllerClass, array $options): PendingSingletonResourceRegistration
    {
        $router = $this->getRouter();
        $registration = $router->singleton($name, $controllerClass, $options);
        return $registration;
    }
}