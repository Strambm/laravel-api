<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Facades\Router;

use Cetria\Laravel\Api\Tests\Unit\Facades\Router\TestCase;
use Illuminate\Routing\PendingResourceRegistration;
use PHPUnit\Framework\Attributes\Test;

class ResourceTest extends TestCase
{
    #[Test]
    public function returnsPendingSingletonResourceRegistration(): void
    {
        $name = 'test';
        $controller = 'TestController';
        $options = ['foo' => 'bar'];

        $result = $this->act($name, $controller, $options);
        $this->assertCompareResourceRegistration($result, $name, $controller, $options);
    }

    protected function act(string $name, string $controllerClass, array $options): PendingResourceRegistration
    {
        $router = $this->getRouter();
        $registration = $router->resource($name, $controllerClass, $options);
        return $registration;
    }
}