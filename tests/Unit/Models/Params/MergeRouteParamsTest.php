<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use Cetria\Laravel\Api\Models\Params;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class MergeRouteParamsTest extends TestCase
{
    #[Test]
    public function emptyBefore(): void
    {
        $params = $this->arrangeParams([]);
        $params = $this->act($params, [
            'key' => 12,
        ]);
        $mergedParams = Reflection::getHiddenProperty($params, 'routeParams');
        $this->assertCount(1, $mergedParams);
        $this->assertArrayHasKey('key', $mergedParams);
        $this->assertEquals(12, $mergedParams['key']);
    }

    #[Test]
    public function same(): void
    {
        $params = $this->arrangeParams([
            'key' => 12,
        ]);
        $params = $this->act($params, [
            'key' => 12,
        ]);
        $mergedParams = Reflection::getHiddenProperty($params, 'routeParams');
        $this->assertCount(1, $mergedParams);
        $this->assertArrayHasKey('key', $mergedParams);
        $this->assertEquals(12, $mergedParams['key']);
    }

    #[Test]
    public function emptyAfter(): void
    {
        $params = $this->arrangeParams([
            'key' => 12,
        ]);
        $params = $this->act($params, []);
        $mergedParams = Reflection::getHiddenProperty($params, 'routeParams');
        $this->assertCount(1, $mergedParams);
        $this->assertArrayHasKey('key', $mergedParams);
        $this->assertEquals(12, $mergedParams['key']);
    }

    #[Test]
    public function twoDifferent(): void
    {
        $params = $this->arrangeParams([
            'key2' => 12,
        ]);
        $params = $this->act($params, [
            'key1' => 13,
        ]);
        $mergedParams = Reflection::getHiddenProperty($params, 'routeParams');
        $this->assertCount(2, $mergedParams);
        $this->assertArrayHasKey('key2', $mergedParams);
        $this->assertEquals(12, $mergedParams['key2']);
        $this->assertArrayHasKey('key1', $mergedParams);
        $this->assertEquals(13, $mergedParams['key1']);
    }

    protected function arrangeParams(array $routeParamsBefore): Params
    {
        $params = new Params();
        Reflection::setHiddenProperty($params, 'routeParams', $routeParamsBefore);
        return $params;
    }

    protected function act(Params $params, array $routeParams): Params
    {
        return $params->mergeRouteParams($routeParams);
    }
}