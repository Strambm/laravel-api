<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use Cetria\Laravel\Api\Models\Params;

class SetExpectedCountTest extends SetterTestCase
{
    protected static function getPropertyName(): string
    {
        return 'expectedCount';
    }

    protected function act(Params $params, mixed $value): Params
    {
        return $params->setExpectedCount($value);
    }

    public static function setterDataProviderMethod(): array
    {
        return [
            [
                [
                    static::PARAM_VALUE_BEFORE => null,
                    static::PARAM_VALUE_AFTER => 8,
                ],
            ], [
                [
                    static::PARAM_VALUE_BEFORE => 1,
                    static::PARAM_VALUE_AFTER => 2,
                ],
            ],
        ];
    }
}