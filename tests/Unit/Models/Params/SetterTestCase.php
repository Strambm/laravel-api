<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Models\Params;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;

abstract class SetterTestCase extends TestCase
{
    const PARAM_VALUE_BEFORE = 'before';
    const PARAM_VALUE_AFTER = 'after';

    #[Test]
    #[DataProvider('setterDataProviderMethod')]
    public function setter(array $data): void
    {
        $params = $this->arrangeParams($data[static::PARAM_VALUE_BEFORE]);
        $result = $this->act($params, $data[static::PARAM_VALUE_AFTER]);
        $this->assertEquals($data[static::PARAM_VALUE_AFTER], Reflection::getHiddenProperty($result, static::getPropertyName()));
    }

    protected function arrangeParams(mixed $expectedValue): Params
    {
        $params = new Params();
        Reflection::setHiddenProperty($params, static::getPropertyName(), $expectedValue);
        return $params;
    }

    abstract protected static function getPropertyName(): string;
    abstract protected function act(Params $params, mixed $value): Params;
    abstract public static function setterDataProviderMethod(): array;
}