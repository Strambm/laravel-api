<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use Cetria\Laravel\Api\Models\Params;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class AddRouteParamTest extends TestCase
{
    #[Test]
    public function basicAdd(): void
    {
        $key = 'test';
        $value = 1;
        $params = $this->arrangeParams();
        $result = $this->act($params, $key, $value);
        $this->assertParamsCount(1, $params);
        $this->assertRouteParamExists($key, $value, $result);
    }

    #[Test]
    public function redundantAdd(): void
    {
        $key = 'test';
        $value = 1;
        $params = $this->arrangeParams([$key => 2]);
        $result = $this->act($params, $key, $value);
        $this->assertParamsCount(1, $params);
        $this->assertRouteParamExists($key, $value, $result);
    }

    #[Test]
    public function moreAdds(): void
    {
        $key = 'test';
        $value = 1;
        $params = $this->arrangeParams(['test2' => 2]);
        $result = $this->act($params, $key, $value);
        $this->assertParamsCount(2, $params);
        $this->assertRouteParamExists('test2', 2, $result);
        $this->assertRouteParamExists($key, $value, $result);
    }

    protected function arrangeParams(array $beforeRouteParams = []): Params
    {
        $params = new Params();
        foreach($beforeRouteParams as $key => $value) {
            $params->addRouteParam($key, $value);
        }
        return $params;
    }

    protected function act(Params $params, string $key, string|int $value): Params
    {
        return $params->addRouteParam($key, $value);
    }

    protected function assertParamsCount(int $expectedCount, Params $params): void
    {
        $routeParams = Reflection::getHiddenProperty($params, 'routeParams');
        $this->assertCount($expectedCount, $routeParams);
    }

    protected function assertRouteParamExists(string $key, string|int $value, Params $params): void
    {
        $routeParams = Reflection::getHiddenProperty($params, 'routeParams');
        $this->assertArrayHasKey($key, $routeParams);
        $this->assertEquals($value, $routeParams[$key]);
    }
}