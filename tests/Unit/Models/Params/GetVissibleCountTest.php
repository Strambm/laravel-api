<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use Cetria\Laravel\Api\Models\Params;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\Test;

class GetVissibleCountTest extends GetterTestCase
{
    #[Test]
    public function withSetExpectedCount(): void
    {
        $params = $this->arrangeParams(50);
        Reflection::setHiddenProperty($params, 'expectedCount', 11);
        $result = $this->act($params);
        $this->assertEquals(11, $result);
    }

    public static function getterDataProviderMethod(): array
    {
        return [
            [
                80,
            ],
        ];
    }

    protected function act(Params $params): int|null
    {
        return $params->getVissibleCount();
    }

    protected static function getPropertyName(): string
    {
        return 'arrangeVissibleItemsCount';
    }
}