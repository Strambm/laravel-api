<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use Cetria\Laravel\Api\Models\Params;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class GetRouteParamsTest extends TestCase
{
    #[Test]
    public function empty(): void
    {
        $params = new Params();
        $result = $this->act($params);
        $this->assertEquals([], $result);
    }

    #[Test]
    public function basicParam(): void
    {
        $params = new Params();
        $params->addRouteParam('idk', 8);
        $result = $this->act($params);
        $this->assertArrayHasKey('idk', $result);
        $this->assertEquals(8, $result['idk']);
    }

    #[Test]
    public function withLimit(): void
    {
        $params = new Params();
        $params->setLimit(55);
        $result = $this->act($params);
        $this->assertArrayHasKey('limit', $result);
        $this->assertEquals(55, $result['limit']);
    }

    #[Test]
    public function withOffset(): void
    {
        $params = new Params();
        $params->setOffset(55);
        $result = $this->act($params);
        $this->assertArrayHasKey('offset', $result);
        $this->assertEquals(55, $result['offset']);
    }

    protected function act(Params $params): array
    {
        return $params->getRouteParams();
    }
}