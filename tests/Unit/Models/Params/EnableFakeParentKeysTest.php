<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Models\Params;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\DataProvider;

class EnableFakeParentKeysTest extends TestCase
{
    const VALUE_BEFORE = 'before';
    const VALUE_AFTER = 'after';

    #[Test]
    #[DataProvider('methodDataProvider')]
    public function testMethod(array $testParams, bool $expectedResult): void
    {
        $params = $this->arrangeParams($testParams);
        $result = $this->act($params, $testParams);
        $this->assertCorrectResult($result, $expectedResult);
    }

    public static function methodDataProvider(): array
    {
        return [
            [
                [
                    self::VALUE_BEFORE => true,
                    self::VALUE_AFTER => true,
                ],
                true,
            ], [
                [
                    self::VALUE_BEFORE => false,
                    self::VALUE_AFTER => true,
                ],
                true,
            ], [
                [
                    self::VALUE_BEFORE => false,
                    self::VALUE_AFTER => false,
                ],
                false,
            ], [
                [
                    self::VALUE_BEFORE => true,
                    self::VALUE_AFTER => false,
                ],
                false,
            ],
        ];
    }

    protected function arrangeParams(array $testParams): Params
    {
        $params = new Params();
        Reflection::setHiddenProperty($params, 'hasFakeParentKeys', $testParams[self::VALUE_BEFORE]);
        return $params;
    }

    protected function act(Params $params, array $testParams): Params
    {
        return $params->enableFakeParentKeys($testParams[self::VALUE_AFTER]);
    }

    protected function assertCorrectResult(Params $params, int $expectedResult): void
    {
        $this->assertEquals(Reflection::getHiddenProperty($params, 'hasFakeParentKeys'), $expectedResult);
    }
}