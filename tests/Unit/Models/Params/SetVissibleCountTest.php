<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use Cetria\Laravel\Api\Models\Params;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\Test;

class SetVissibleCountTest extends SetterTestCase
{
    #[Test]
    public function lowerLimit(): void
    {
        $params = $this->arrangeParams(1);
        $result = $this->act($params, -1);
        $this->assertEquals(0, Reflection::getHiddenProperty($result, static::getPropertyName()));
    }

    protected static function getPropertyName(): string
    {
        return 'arrangeNoiseCount';
    }

    protected function act(Params $params, mixed $value): Params
    {
        return $params->setNoiseCount($value);
    }

    public static function setterDataProviderMethod(): array
    {
        return [
            [
                [
                    static::PARAM_VALUE_BEFORE => 0,
                    static::PARAM_VALUE_AFTER => 50,
                ],
            ], [
                [
                    static::PARAM_VALUE_BEFORE => 50,
                    static::PARAM_VALUE_AFTER => 2,
                ],
            ], [
                [
                    static::PARAM_VALUE_BEFORE => 50,
                    static::PARAM_VALUE_AFTER => 0,
                ],
            ],
        ];
    }
}