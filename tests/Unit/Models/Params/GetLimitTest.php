<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use Cetria\Laravel\Api\Models\Params;

class GetLimitTest extends GetterTestCase
{
    public static function getterDataProviderMethod(): array
    {
        return [
            [
                null,
            ], [
                149,
            ],
        ];
    }

    protected function act(Params $params): int|null
    {
        return $params->getLimit();
    }

    protected static function getPropertyName(): string
    {
        return 'limit';
    }
}