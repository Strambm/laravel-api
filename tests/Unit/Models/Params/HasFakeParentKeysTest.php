<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use Cetria\Laravel\Api\Models\Params;

class HasFakeParentKeysTest extends GetterTestCase
{
    public static function getterDataProviderMethod(): array
    {
        return [
            [
                true,
            ], [
                false,
            ],
        ];
    }

    protected function act(Params $params): int|null
    {
        return $params->hasFakeParentKeys();
    }

    protected static function getPropertyName(): string
    {
        return 'hasFakeParentKeys';
    }
}