<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Api\Models\Params;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\DataProvider;

abstract class GetterTestCase extends TestCase
{
    #[Test]
    #[DataProvider('getterDataProviderMethod')]
    public function getter(mixed $expectedValue): void
    {
        $params = $this->arrangeParams($expectedValue);
        $result = $this->act($params);
        $this->assertEquals($expectedValue, $result);
    }

    protected function arrangeParams(mixed $expectedValue): Params
    {
        $params = new Params();
        Reflection::setHiddenProperty($params, static::getPropertyName(), $expectedValue);
        return $params;
    }

    abstract protected static function getPropertyName(): string;
    abstract protected function act(Params $params): mixed;
    abstract public static function getterDataProviderMethod(): array;
}