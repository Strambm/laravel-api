<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use Cetria\Laravel\Api\Models\Params;

class SetLimitTest extends SetterTestCase
{
    protected static function getPropertyName(): string
    {
        return 'limit';
    }

    protected function act(Params $params, mixed $value): Params
    {
        return $params->setLimit($value);
    }

    public static function setterDataProviderMethod(): array
    {
        return [
            [
                [
                    static::PARAM_VALUE_BEFORE => null,
                    static::PARAM_VALUE_AFTER => 50,
                ],
            ], [
                [
                    static::PARAM_VALUE_BEFORE => 50,
                    static::PARAM_VALUE_AFTER => 2,
                ],
            ], [
                [
                    static::PARAM_VALUE_BEFORE => 50,
                    static::PARAM_VALUE_AFTER => null,
                ],
            ],
        ];
    }
}