<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\Params;

use Cetria\Laravel\Api\Models\Params;

class GetNoiseCountTest extends GetterTestCase
{
    public static function getterDataProviderMethod(): array
    {
        return [
            [
                0,
            ], [
                2,
            ],
        ];
    }

    protected function act(Params $params): int|null
    {
        return $params->getNoiseCount();
    }

    protected static function getPropertyName(): string
    {
        return 'arrangeNoiseCount';
    }
}