<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\State;

use Cetria\Laravel\Api\Models\State;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class ConstructorTest extends TestCase
{
    #[Test]
    public function withoutParams(): void
    {
        $name = 'testName';
        $state = new State($name);
        $this->assertEquals($name, Reflection::getHiddenProperty($state, 'name'));
        $this->assertCount(0, Reflection::getHiddenProperty($state, 'params'));
    }

    #[Test]
    public function moreParams(): void
    {
        $name = 'testName';
        $state = new State($name, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        $this->assertEquals($name, Reflection::getHiddenProperty($state, 'name'));
        $this->assertCount(15, Reflection::getHiddenProperty($state, 'params'));
    }
}