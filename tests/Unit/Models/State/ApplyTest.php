<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\State;

use Cetria\Laravel\Api\Models\State;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Factories\Factory;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class ApplyTest extends TestCase
{
    #[Test]
    public function apply(): void
    {
        $factory = $this->arrangeFactory();
        $state = new State('testScope', 1, 'test');
        $updatedFactory = $state->apply($factory);
        $states = Reflection::getHiddenProperty($updatedFactory, 'states');
        $this->assertCount(1, $states);
    }

    protected function arrangeFactory(): Factory
    {
        $factory = new class extends Factory
        {
            public function definition()
            {
                return [];
            }

            public function testScope(int $param1, string $param2): static
            {
                return $this->state([
                    
                ]);
            }
        };
        return $factory;
    }
}