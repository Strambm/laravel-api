<?php 
namespace Cetria\Laravel\Api\Tests\Unit\Models\ExportDrivers\CSV;

use Cetria\Laravel\Api\Models\ExportDrivers\CSV;
use Mockery;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Builder;
use Cetria\Laravel\Api\Tests\Dummy\Product;
use Cetria\Laravel\Api\Tasks\MakeExportParams;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;

class MakeFileTest extends TestCase
{
    #[Test]
    public function handle(): void
    {
        Product::factory()->count(10)->create();
        $query = Product::query();
        $result = $this->act(builder: $query);
        $this->assertEquals('TEST.csv', $result);
    }

    private function act(Builder $builder): string
    {
        $driver = $this->getDriver();
        /** @var MockObject|MakeExportParams $exportParams */
        $exportParams = $this->getMockBuilder(MakeExportParams::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getItemsOnPage'])
            ->getMock();
        $exportParams->expects($this->any())
            ->method('getItemsOnPage')
            ->willReturn(1);
        return $driver->makeFile($builder, $exportParams);
    }

    protected function setUp(): void
    {
        parent::setUp();
        DummyHelper::init();
        $driver = $this->getDriver();
        $getStorageDriverNameMethod = Reflection::getHiddenMethod($driver, 'getStorageDriverName');
        $driverName = $getStorageDriverNameMethod->invoke($driver);
        $storageMock = Mockery::mock('alias:Illuminate\Support\Facades\Storage');
        $storageMock->shouldReceive('drive')
            ->times(2)
            ->with($driverName)
            ->andReturnSelf();

        $storageMock->shouldReceive('put')
            ->once()
            ->withArgs(function ($filename, $csvString): bool {
                $this->assertEquals('TEST.csv', $filename);
                $lines = explode("\n", $csvString);
                $array = array_map('str_getcsv', $lines);
                array_pop($array);
                $this->assertEquals(11, count($array));//10+hlavička
                $firstModel = Product::query()->first();
                $modelData = $firstModel->toArray();
                foreach($modelData as $key => $value) {
                    $csvKey = array_search($key, $array[0]);
                    $this->assertEquals((string) $value, $array[1][$csvKey], 'key: \'' . $key . '\'');
                }
                return true;
            });

        $storageMock->shouldReceive('path')
            ->once()
            ->andReturn('TEST.csv');
    }

    private function getDriver(): CSV
    {
        $driver = $this->getMockBuilder(CSV::class)
            ->onlyMethods(['generateFilename'])
            ->getMock();
        $driver->expects($this->any())
            ->method('generateFilename')
            ->willReturn('TEST.csv');
        return $driver;
    }

    protected function tearDown(): void
    {
        Mockery::close();
    }
}