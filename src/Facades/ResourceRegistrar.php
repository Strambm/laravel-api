<?php

namespace Cetria\Laravel\Api\Facades;

use Illuminate\Routing\ResourceRegistrar as BaseResourceRegistrar;

class ResourceRegistrar extends BaseResourceRegistrar
{
    protected $resourceDefaults = [
        'index', 
        'indexExport', 
        'create', 
        'store', 
        'storeValidate',
        'show', 
        'edit', 
        'update', 
        'updateValidate',
        'destroy',
    ];

    protected static $verbs = [
        'create' => 'create',
        'edit' => 'edit',
        'indexExport' => 'indexExport',
        'storeValidate' => 'storeValidate',
        'updateValidate' => 'updateValidate',
    ];

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Facades\ResourceRegistratar\AddResourceIndexExportTest
     */
    protected function addResourceIndexExport($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name). '/' . static::$verbs['indexExport'];
        $action = $this->getResourceAction($name, $controller, 'indexExport', $options);
        return $this->router->post($uri, $action);
    }

    protected function addResourceStoreValidate($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name). '/' . static::$verbs['storeValidate'];
        $action = $this->getResourceAction($name, $controller, 'storeValidate', $options);
        return $this->router->post($uri, $action);
    }

    protected function addResourceUpdateValidate($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name). '/' . static::$verbs['updateValidate'];
        $action = $this->getResourceAction($name, $controller, 'updateValidate', $options);
        return $this->router->post($uri, $action);
    }
}
