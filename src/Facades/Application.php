<?php

namespace Cetria\Laravel\Api\Facades;

use Cetria\Laravel\Api\Providers\CustomRouteServiceProvider;
use Illuminate\Foundation\Application as FoundationApplication;

class Application extends FoundationApplication
{
    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Facades\Application\RegisterBaseServiceProvidersTest
     */
    protected function registerBaseServiceProviders()
    {
        parent::registerBaseServiceProviders();
        $this->register(new CustomRouteServiceProvider($this));
    }
}
