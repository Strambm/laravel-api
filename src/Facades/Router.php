<?php

namespace Cetria\Laravel\Api\Facades;

use Illuminate\Routing\Router as RoutingRouter;
use Illuminate\Routing\PendingResourceRegistration;
use Illuminate\Routing\PendingSingletonResourceRegistration;

class Router extends RoutingRouter
{
    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Facades\Router\SingletonTest
     */
    public function singleton($name, $controller, array $options = [])
    {
        if ($this->container && $this->container->bound(ResourceRegistrar::class)) {
            $registrar = $this->container->make(ResourceRegistrar::class);
        } else {
            $registrar = new ResourceRegistrar($this);
        }

        return new PendingSingletonResourceRegistration(
            $registrar, $name, $controller, $options
        );
    }
    
    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Facades\Router\ResourceTest
     */
    public function resource($name, $controller, array $options = [])
    {
        if ($this->container && $this->container->bound(ResourceRegistrar::class)) {
            $registrar = $this->container->make(ResourceRegistrar::class);
        } else {
            $registrar = new ResourceRegistrar($this);
        }

        return new PendingResourceRegistration(
            $registrar, $name, $controller, $options
        );
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Facades\Router\SingularResourceParametersTest
     */
    public function singularResourceParameters($singular = true)
    {
        ResourceRegistrar::singularParameters($singular);
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Facades\Router\ResourceParametersTest
     */
    public function resourceParameters(array $parameters = [])
    {
        ResourceRegistrar::setParameters($parameters);
    }

    public function resourceVerbs(array $verbs = [])
    {
        return ResourceRegistrar::verbs($verbs);
    }
}
