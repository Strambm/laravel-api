<?php

namespace Cetria\Laravel\Api\Helpers;

class AuthHelper
{
    static protected $token = null;

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Helpers\AuthHelper\SetTokenTest
     */
    static function setToken(string $token): void
    {
        static::$token = $token;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Helpers\AuthHelper\LogoutTest
     */
    static function logout(): void
    {
        static::$token = null;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Helpers\AuthHelper\GetTokenTest
     */
    static function getToken(): string
    {
        return 'Bearer ' . static::$token;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Helpers\AuthHelper\HasTokenTest
     */
    static function hasToken(): bool
    {
        return !is_null(static::$token);
    }
}
