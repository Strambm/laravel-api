<?php

namespace Cetria\Laravel\Api\Providers;

use Cetria\Laravel\Api\Facades\Router;
use App\Providers\RouteServiceProvider;

class CustomRouteServiceProvider extends RouteServiceProvider
{
    public function register()
    {
        parent::register();
        $this->app->singleton('router', function ($app) {
            return new Router($app['events'], $app);
        });
    }
}
