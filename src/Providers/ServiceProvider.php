<?php

namespace Cetria\Laravel\Api\Providers;

use Illuminate\Support\ServiceProvider as BasicServiceProvider;

class ServiceProvider extends BasicServiceProvider
{
    public function boot(): void
    {
        $this->publishes([
            __DIR__.'/../Config/CetriaHasCrud.php' => config_path('cetriaHasCrud.php'),
        ]);
    }
}
