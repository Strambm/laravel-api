<?php

namespace Cetria\Laravel\Api\Interfaces;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

interface CrudControllerInterface
{
    public function index(Request $request, int ...$ids): Response;
    public function indexExport(Request $request, int ...$ids): Response;
    public function create(Request $request, int ...$ids): Response;
    public function store(Request $request, int ...$ids): Response;
    public function storeValidate(Request $request, int ...$ids): Response;
    public function show(Request $request, int ...$ids): Response;
    public function edit(Request $request, int ...$ids): Response;
    public function update(Request $request, int ...$ids): Response;
    public function updateValidate(Request $request, int ...$ids): Response;
    public function destroy(Request $request, int ...$ids): Response;
}
