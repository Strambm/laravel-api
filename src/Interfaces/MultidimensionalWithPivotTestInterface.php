<?php

namespace Cetria\Laravel\Api\Interfaces;

use Illuminate\Contracts\Auth\Authenticatable;

interface MultidimensionalWithPivotTestInterface
{
    public function makeTestUser(): Authenticatable;
}
