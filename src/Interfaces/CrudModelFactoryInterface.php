<?php

namespace Cetria\Laravel\Api\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface CrudModelFactoryInterface
{
    public function forModel(Model $model): static;
}
