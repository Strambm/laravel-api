<?php

namespace Cetria\Laravel\Api\Interfaces;

interface MultidimensionalInterface
{
    public function getTestsControllerBefore(): string;
}
