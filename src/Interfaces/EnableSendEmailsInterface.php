<?php

namespace Cetria\Laravel\Api\Interfaces;

interface EnableSendEmailsInterface
{
    public function getEmailClass(): string;
}
