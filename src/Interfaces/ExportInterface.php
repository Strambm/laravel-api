<?php

namespace Cetria\Laravel\Api\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use Cetria\Laravel\Api\Tasks\MakeExportParams;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface ExportInterface
{
    /**
     * @return string storedFilename
     */
    public function makeFile(Builder|BelongsToMany $builder, MakeExportParams $params): string;
}
