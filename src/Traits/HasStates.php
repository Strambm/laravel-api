<?php

namespace Cetria\Laravel\Api\Traits;

use Cetria\Laravel\Api\Models\State;

trait HasStates
{
    /** @var State[] $states */
    protected $states = [];

    public function addState(State $state): self
    {
        $this->states[] = $state;
        return $this;
    }

    /**
     * @return State[]
     */
    public function getStates(): array
    {
        return $this->states;
    }

    public function setStates(array $states): self
    {
        $this->states = $states;
        return $this;
    }
}
