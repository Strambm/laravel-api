<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use Cetria\Laravel\Form\Form;
use Illuminate\Http\Response;
use Cetria\Laravel\Api\Models\Params;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;
use Illuminate\Database\Eloquent\Model;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

trait StoreValidate
{
    use Arranges;
    use Helpers;

    protected $params = [];

    public function storeValidateOk(Params $param): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeAttributes($param);
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_OK);
        return $response;
    }

    public function storeValidateUnauthorized(Params $param): TestResponse
    {
        $this->arrangeAttributes($param);
        AuthHelper::logout();
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        return $response;
    }

    public function storeValidateUnvalid(Params $param): TestResponse
    {
        $model = $this->arrangeVissible($param);
        $model->delete();
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        return $response;
    }

    public function storeValidateNotAllowed(Params $param): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeAttributes($param);
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
        return $response;
    }

    protected function arrangeAttributes(Params &$params): void
    {
        $model = $this->arrangeVissible($params);
        $model->delete();
        $modelInstance = $this->getEmptyModelInstance();
        /** @var Form $form */
        $form = $modelInstance->toForm();
        /** @var Input[] $attributes */
        $attributes = $form->getContent();
        foreach($attributes as $attribute) {
            $paramName = $attribute->getName();
            $this->params[$paramName] = $model->$paramName;
        }
    }

    private function getEmptyModelInstance(): Model
    {
        $modelClass = $this->getParrentModelClass();
        return new $modelClass();
    }

    protected function act(Params $params): TestResponse
    {
        Auth::logout();
        $response = $this->json('POST', $this->getRoute('storeValidate', $params->getRouteParams()), $this->params, $this->getHeader());
        return $response;
    }

    public function getParamsForCorrectGenerate(): Params
    {
        return (new Params())
            ->setVissibleCount(1);
    }

    protected function loginConditional(): bool
    {
        return !Auth::check();
    }
}
