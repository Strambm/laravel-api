<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use Cetria\Laravel\Api\Traits\HasCrudViaPivot;
use Illuminate\Http\Response;
use Cetria\Laravel\Api\Models\Params;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\SoftDeletes;

trait Destroy
{
    use Arranges;
    use Helpers;

    protected function destroyOk(Params $params): TestResponse
    {
        $model = $this->arrangeVissible($params);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        if(!in_array(HasCrudViaPivot::class, Reflection::classUsesRecursive($this->getControllerClass()))) {
            $this->assertDeleted($model);
        } else {
            $params->getRouteParams();
            $response2 =  $response = $this->json('POST', $this->getRoute('destroy', $params->getRouteParams()), ['_method' => 'DELETE'], $this->getHeader());
            $response2->assertStatus(Response::HTTP_NOT_FOUND);
            $model::class::withoutGlobalScopes()->findOrFail($model->getKey());
        }
        return $response;
    }

    protected function destroyNotFound(Params $params): TestResponse
    {
        $model = $this->arrangeVissible($params);
        $model->delete();
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        return $response;
    }

    protected function destroyUnauthorized(Params $params): TestResponse
    {
        $model = $this->arrangeVissible($params);
        $model->delete();
        $this->logout();
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        return $response;
    }

    protected function destroyNotAllowed(Params $params): TestResponse
    {
        $model = $this->arrangeVissible($params);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
        return $response;
    }

    protected function act(Params $params): TestResponse
    {
        Auth::logout();
        $response = $this->json('POST', $this->getRoute('destroy', $params->getRouteParams()), ['_method' => 'DELETE'], $this->getHeader());
        return $response;
    }

    protected function assertDeleted(Model $model): void
    {
        $params = [
            $model->getKeyName() => $model->getKey(),
        ];
        if(in_array(SoftDeletes::class ,Reflection::classUsesRecursive($model))) {
            $this->assertSoftDeleted($model->getTable(), $params);
        } else {
            $this->assertDatabaseMissing($model->getTable(), $params);
        }
    }

    public function getParamsForCorrectGenerate(): Params
    {
        return (new Params())
            ->setVissibleCount(1);
    }

    protected function loginConditional(): bool
    {
        return true
                && !Auth::check();
    }
}
