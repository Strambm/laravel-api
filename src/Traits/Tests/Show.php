<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use Illuminate\Http\Response;
use Cetria\Laravel\Api\Models\Params;
use Cetria\Laravel\Form\Method;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;

trait Show
{
    use Arranges;
    use Helpers;

    protected function showOk(Params $params): TestResponse
    {
        $model = $this->arrangeVissible($params);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertResponseMessage($response, $model);
        return $response;
    }

    protected function showNotFound(Params $params): TestResponse
    {
        $model = $this->arrangeVissible($params);
        $model->delete();
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        return $response;
    }

    protected function showUnauthorized(Params $params): TestResponse
    {
        $model = $this->arrangeVissible($params);
        $model->delete();
        $this->logout();
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        return $response;
    }

    protected function showNotAllowed(Params $params): TestResponse
    {
        $model = $this->arrangeVissible($params);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
        return $response;
    }

    protected function act(Params $params): TestResponse
    {
        Auth::logout();
        $response = $this->json(Method::GET->name, $this->getRoute('show', $params->getRouteParams()), [], $this->getHeader());
        return $response;
    }

    public function getParamsForCorrectGenerate(): Params
    {
        return (new Params())
            ->setVissibleCount(1);
    }
}
