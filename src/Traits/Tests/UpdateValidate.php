<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use ArrayAccess;
use Cetria\Laravel\Form\Form;
use Illuminate\Http\Response;
use Cetria\Laravel\Api\Models\Params;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Hidden;

trait UpdateValidate
{
    use Arranges;
    use Helpers;

    protected $params = [];

    public function updateValidateOk(Params $param): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeAttributes($param);
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_OK);
        return $response;
    }

    public function updateValidateUnauthorized(Params $param): TestResponse
    {
        $this->arrangeAttributes($param);
        AuthHelper::logout();
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        return $response;
    }

    public function updateValidateUnvalid(Params $param): TestResponse
    {
        AuthHelper::logout();
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_CONFLICT);
        return $response;
    }

    protected function updateValidateNotAllowed(Params $param): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeAttributes($param);
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
        return $response;
    }

    protected function arrangeAttributes(Params &$params): Model
    {
        $models = $this->arrangeVissibles($params);
        $main = $models->first();
        $temp = $models->last();
        $temp->delete();
        /** @var Form $form */
        $form = $main->toForm();
        $attributes = $form->getContent();
        $attributes = array_filter($attributes, function(Input $attribute) use ($temp): bool {
            $name = $attribute->getName();
            $attributes = $temp->getAttributes();
            return $attribute instanceof Hidden === false
                && $attributes[$name] != $attribute->getValue();
        });
        $randIndex = array_rand($attributes);
        $attribute = $attributes[$randIndex];
        /** @var Input $value */
        foreach($form->getContent() as $value) {
            $this->params[$value->getName()] = Reflection::getHiddenProperty($value, 'value');
            if($value->getName() == $randIndex) {
                $this->params[$value->getName()] = Reflection::getHiddenProperty($attribute, 'value');
            }
        }
        return $main;
    }

    protected function act(Params $params): TestResponse
    {
        Auth::logout();
        $response = $this->json('POST', $this->getRoute('updateValidate', $params->getRouteParams()), $this->params, $this->getHeader());
        return $response;
    }

    public function getParamsForCorrectGenerate(): Params
    {
        return (new Params())
            ->setVissibleCount(2);
    }

    protected function loginConditional(): bool
    {
        return !Auth::check();
    }

    abstract public static function assertArrayHasKey(string|int $key, ArrayAccess|array $array, string $message = ''): void;
    abstract public static function assertEquals($expected, $actual, string $message = ''): void;
    abstract public static function assertNotEmpty($actual, string $message = ''): void;
}
