<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use Illuminate\Http\Response;
use Cetria\Laravel\Api\Models\Params;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;
use Illuminate\Database\Eloquent\Collection;

trait Create
{
    use Arranges {
        arrangeVissibles as arrangesArrangeVissibles;
    }
    use Helpers;

    protected function createOk(Params $params): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeVissibles($params);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_OK);
        return $response;
    }

    protected function createUnauthorized(Params $params): TestResponse
    {
        $this->arrangeVissibles($params);
        AuthHelper::logout();
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        return $response;
    }

    protected function createNotAllowed(Params $params): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeVissibles($params);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
        return $response;
    }

    public function basicArrangeVissibles(Params &$params): Collection
    {  
        return new Collection([]);
    }

    public function arrangeVissibles(Params &$params, int $deep = 0): Collection
    {  
        if($this->isTestedControllerMultidimensional()) {
            if($deep == 0) {
                $this->arrangeVisibleModelsForParent($params, ++ $deep);
            } else {
                return $this->arrangesArrangeVissibles($params, $deep);
            }
        } elseif($deep != 0) {
            return $this->arrangesArrangeVissibles($params, $deep);
        }
        return new Collection([]);
    }

    protected function hasGeneratedModelKeyInUrl(int $deep = 0): bool
    {
        return $deep != 0;
    }

    protected function act(Params $params): TestResponse
    {
        Auth::logout();
        $response = $this->json('GET', $this->getRoute('create', $params->getRouteParams()), [], $this->getHeader());
        return $response;
    }

    public function getParamsForCorrectGenerate(): Params
    {
        return (new Params());
    }

    protected function loginConditional(): bool
    {
        return true
                && !Auth::check();
    }
}
