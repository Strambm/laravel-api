<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use Illuminate\Contracts\Auth\Authenticatable;

trait HasAuthenticatedUser
{
    abstract protected function getAuthUser(): Authenticatable;
}
