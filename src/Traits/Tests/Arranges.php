<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use Cetria\Laravel\Api\Interfaces\CrudModelFactoryInterface;
use Exception;
use Cetria\Laravel\Api\Models\State;
use Cetria\Laravel\Api\Models\Params;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Collection;
use PHPUnit\Framework\MockObject\MockBuilder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Cetria\Laravel\Api\Interfaces\MultidimensionalInterface;

trait Arranges
{
    abstract public function getMockBuilder(string $className): MockBuilder;
    
    public function arrangeVissible(Params &$params): Model
    {  
        return $this->arrangeVissibles($params)
            ->first();
    }

    /**
     * @see \Cetria\Laravel\Api\Traits\Tests\Arranges\ArrangeVissiblesTest
     */
    protected function arrangeVissibles(Params &$params, int $deep = 0): Collection
    {  
        if($this->isTestedControllerMultidimensional()) {
            $parentArrangedModel = $this->arrangeVisibleModelsForParent($params, $deep);
            $arrangeParams = clone $params;
            $arrangeParams->addState(new State('forModel', $parentArrangedModel));
            $result = $this->basicArrangeVissibles($arrangeParams);
        } else {
            $result = $this->basicArrangeVissibles($params);
        }
        if($this->hasGeneratedModelKeyInUrl($deep)) {
            $params->addRouteParam($this->getActualRouteParamName(), $this->getUrlKey($params, $deep, $result));
        }
        return $result;
    }

    protected function isTestedControllerMultidimensional(): bool
    {
        return $this instanceof MultidimensionalInterface;
    }

    protected function arrangeVisibleModelsForParent(Params &$params, int $deep = 0): Model
    {
        $controllerBefore = $this->getTestControllerBeforeInstance();
        $paramsBefore = ($controllerBefore->getParamsForCorrectGenerate())
            ->setVissibleCount(1);
        $methodName = 'arrangeVissibles';
        $method = Reflection::getHiddenMethod($controllerBefore, $methodName);
        $parentArrangedModel = $method->invokeArgs($controllerBefore, [&$paramsBefore, ++ $deep])->first();
        $params->mergeRouteParams($paramsBefore->getRouteParams());
        return $parentArrangedModel;
    }

    protected function getTestControllerBeforeInstance()
    {
        $controllerClass = $this->getTestsControllerBefore();
        $testController = $this->getMockBuilder($controllerClass)
            ->onlyMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        return $testController;
    }

    public function basicArrangeVissibles(Params &$params): Collection
    {  
        $models = $this->arrangeModels($params, 'getVissibleCount');
        return $models;
    }

    protected function arrangeModels(Params $params, string $countMethod): Collection
    {
        $this->loginTestUserIfCan();
        $collection = new Collection();
        for($counter = 0; $params->$countMethod() > $counter; $counter ++) {
            $newModel = $this->arrangeModel($params);
            $collection->push($newModel);
        }
        return $collection;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Arranges\ArrangeNoiseTest
     */
    protected function arrangeNoise(Params $params): void
    {
        $this->arrangeModels($params, 'getNoiseCount');
    }

    protected function arrangeModel(Params $params, array $factoryParams = []): Model
    {
        $modelClass = $this->getParrentModelClass();
        if(!is_subclass_of($modelClass, Model::class)) {
            throw new Exception($modelClass . ' is not instance of \'' . Model::class . '\'');
        }
        $factory = $this->getFactoryForModelClass($modelClass);
        foreach($params->getStates() as $state) {
            $factory = $state->apply($factory);
        }
        $model = $factory
            ->create($factoryParams);
            
        $model = $model::withoutGlobalScopes()->findOrFail($model->getKey());
        return $model;
    }

    private function getFactoryForModelClass(string $modelClass): Factory
    {
        $factory = $modelClass::factory();
        if(
            $factory instanceof CrudModelFactoryInterface == false
                && $this->isTestedControllerMultidimensional()
        ) {
            throw new Exception(get_class($factory) . ' is not instance of \'' . CrudModelFactoryInterface::class . '\'');
        }
        return $factory;
    }

    protected function hasGeneratedModelKeyInUrl(int $deep = 0): bool
    {
        return true;
    }

    protected function getUrlKey(Params $params, int $deep, Collection $result): int|string
    {
        if(
            $params->hasFakeParentKeys()
                && $deep != 0
        ) {
            return random_int(1, PHP_INT_MAX);
        } else {
            return $result->first()->getKey();
        }
    }

    protected abstract function getParrentModelClass(): string;

    abstract protected function getControllerClass(): string;

    protected abstract function getActualRouteParamName(): string;

    public abstract function getParamsForCorrectGenerate(): Params;

    abstract protected function loginTestUserIfCan(): void;
}
