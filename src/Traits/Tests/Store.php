<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use ArrayAccess;
use Illuminate\Support\Str;
use Cetria\Laravel\Form\Form;
use Illuminate\Http\Response;
use Cetria\Laravel\Api\Models\Params;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;
use Illuminate\Database\Eloquent\Model;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

trait Store
{
    use Arranges;
    use Helpers;

    protected $params = [];

    public function storeOk(Params $param): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeAttributes($param);
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertCompareParamsWithResponse($response);
        $this->assertCompareParamsWithStoredModel($response);
        return $response;
    }

    public function storeUnauthorized(Params $param): TestResponse
    {
        $this->arrangeAttributes($param);
        AuthHelper::logout();
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        return $response;
    }

    public function storePivotByRelatedId(Params $param): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeAttributes($param);
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_CREATED);
        return $response;
    }

    public function storeNotAllowed(Params $param): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeAttributes($param);
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
        return $response;
    }

    protected function arrangeAttributes(Params &$params): void
    {
        $model = $this->arrangeVissible($params);
        $model->delete();
        $modelInstance = $this->getEmptyModelInstance();
        /** @var Form $form */
        $form = $modelInstance->toForm();
        /** @var Input[] $attributes */
        $attributes = $form->getContent();
        foreach($attributes as $attribute) {
            $paramName = $attribute->getName();
            $this->params[$paramName] = $model->$paramName;
        }
    }

    protected function arrangeAttributesForPivotByRelatedId(Params &$params): void
    {
        $model = $this->arrangeVissible($params);
        $modelInstance = $this->getEmptyModelInstance();
        /** @var Form $form */
        $form = $modelInstance->toForm();
        /** @var Input[] $attributes */
        $attributes = $form->getContent();
        foreach($attributes as $attribute) {
            $paramName = $attribute->getName();
            if(Str::startsWith('pivot.', $paramName) || $paramName == $model->getKeyName()) {
                $this->params[$paramName] = $model->$paramName;
            }
        }
    }

    private function getEmptyModelInstance(): Model
    {
        $modelClass = $this->getParrentModelClass();
        return new $modelClass();
    }

    protected function act(Params $params): TestResponse
    {
        Auth::logout();
        $response = $this->json('POST', $this->getRoute('store', $params->getRouteParams()), $this->params, $this->getHeader());
        return $response;
    }

    protected function assertCompareParamsWithResponse(TestResponse $response): void
    {
        $jsonContent = $response->getContent();
        $returnedModel = json_decode($jsonContent, true)['message'];
        $this->assertArrayHasKey($this->getEmptyModelInstance()->getKeyName(), $returnedModel);
        $this->assertNotEmpty($returnedModel[$this->getEmptyModelInstance()->getKeyName()]);
        foreach($this->params as $name => $value) {
            if(array_key_exists($name, $returnedModel)) {
                $this->assertEquals($value, $returnedModel[$name], $name);
            }
        }
    }

    protected function assertCompareParamsWithStoredModel(TestResponse $response): void
    {
        $jsonContent = $response->getContent();
        $returnedModel = json_decode($jsonContent, true)['message'];
        $modelClass = $this->getParrentModelClass();
        $storedModelAttributes = $modelClass::withoutGlobalScopes()
            ->findOrFail($returnedModel[$this->getEmptyModelInstance()->getKeyName()])
            ->getAttributes();
        foreach($this->params as $name => $value) {
            $this->assertArrayHasKey($name, $storedModelAttributes);
            if(is_array($value)) {
                $this->assertEquals(json_decode(json_encode($value), true), json_decode($storedModelAttributes[$name], true));
            } else {
                $this->assertEquals($value, $storedModelAttributes[$name]);
            }
        }
    }

    public function getParamsForCorrectGenerate(): Params
    {
        return (new Params())
            ->setVissibleCount(1);
    }

    protected function loginConditional(): bool
    {
        return !Auth::check();
    }

    abstract public static function assertArrayHasKey(string|int $key, ArrayAccess|array $array, string $message = ''): void;
    abstract public static function assertEquals($expected, $actual, string $message = ''): void;
    abstract public static function assertNotEmpty($actual, string $message = ''): void;
}
