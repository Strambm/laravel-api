<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use ArrayAccess;
use SimpleXMLElement;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Testing\TestResponse;
use Cetria\Laravel\Api\Models\Params;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Api\Tasks\SendExportEmail;

trait IndexExport
{
    use Index;

    protected $routeToPing = 'https://www.google.com';

    protected function indexOk(Params $params): TestResponse
    {
        $this->fakeHttp();
        Storage::fake();
        $this->cleanItemsBefeoreTesting();
        $models = $this->arrangeIndexData($params);
        $models = $this->filterExpectedResult($params, $models);
        $response = $this->act($params, ['urlToPing' => $this->routeToPing]);
        $response->assertStatus(Response::HTTP_ACCEPTED);
        $this->assertCompareReturnRequest($models);
        return $response;
    }

    protected function indexOkViaEmail(Params $params): TestResponse
    {
        Mail::fake();
        $this->cleanItemsBefeoreTesting();
        $models = $this->arrangeIndexData($params);
        $models = $this->filterExpectedResult($params, $models);
        $response = $this->act($params, ['email' => 'b@b.com']);
        $response->assertStatus(Response::HTTP_ACCEPTED);
        $this->assertCompareReturnEmail('b@b.com', $models);

        return $response;
    }

    protected function indexOkWithFilter(Params $params): TestResponse
    {
        $this->fakeHttp();
        $this->cleanItemsBefeoreTesting();
        $models = $this->arrangeIndexData($params);
        $params = $this->arrangeFilter($params, $models);
        $models = $this->filterExpectedResult($params, $models);
        $response = $this->act($params, ['urlToPing' => $this->routeToPing]);
        $response->assertStatus(Response::HTTP_ACCEPTED);
        $this->assertCompareReturnRequest($models);
        return $response;
    }

    protected function indexNotAllowed(Params $params): TestResponse
    {
        $this->fakeHttp();
        Storage::fake();
        $this->cleanItemsBefeoreTesting();
        $models = $this->arrangeIndexData($params);
        $models = $this->filterExpectedResult($params, $models);
        $response = $this->act($params, ['urlToPing' => $this->routeToPing]);
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
        return $response;
    }

    protected function fakeHttp(): void
    {
        Http::fake([
            $this->routeToPing => Http::response(null, 200)
        ]);
    }

    protected function act(Params $params, array $data = []): TestResponse
    {
        Auth::logout();
        $data = array_merge(['format' => 'xml'], $data);
        $response = $this->json('POST', $this->getRoute('indexExport', $params->getRouteParams()), $data, $this->getHeader());
        return $response;
    }

    protected function assertCompareReturnRequest(Collection $models): void
    {   
        /** @var Request $request */
        $request = Http::recorded()->first()[0];
        $this->assertEquals('POST', $request->method());
        $this->assertEquals($this->routeToPing, $request->url());
        $data = $request->data();
        $this->assertArrayHasKey('exportUrl', $data);
        $filePath = config('cetriaHasCrud.feedStoragePath') . '/' . str_replace(config('cetriaHasCrud.feedUrlPrefix'), '', $data['exportUrl']);
        $this->assertFileExists($filePath);
        $this->assertCompareXml(file_get_contents($filePath), $models);
    }

    protected function assertCompareReturnEmail(string $email, Collection $models): void
    {
        Mail::assertSent(function(SendExportEmail $mail) use ($email, $models): bool {
            if($mail->hasTo($email)) {
                $this->assertCompareXml(file_get_contents($mail->data['attachmentPath']), $models);
                return true;
            }
            return false;
        });
    }

    protected function assertCompareXml(string $xml, Collection $models): void
    {
        $xml = new SimpleXMLElement($xml);
        $class = $this->getParrentModelClass();
        $key = (new $class)->getKeyName();
        $this->assertCount($models->count(), $xml);
        foreach($xml->item as $item) {
            $data = $this->makeItemFromXml($item);
            $keyValue = $data[$key];
            $model = $models->where($key, $keyValue)->first();
            $modelToArray = $model->toArray();
            $this->assertCompareArrayModels($modelToArray, $data);
        }
    }

    private function makeItemFromXml(SimpleXMLElement $xml): array
    {
        $data = [];
        $attributes = get_object_vars($xml);
        foreach($attributes as $attribute => $value) {
            if(Str::startsWith($attribute, 'key_')) {
                $attribute = Str::replaceFirst('key_', '', $attribute);
            }
            if($value instanceof SimpleXMLElement) {
                $data[$attribute] = $this->makeItemFromXml($value);
            } else {
                $data[$attribute] = $value;
            }
        }
        return $data;
    }

    private function assertCompareArrayModels(array $expected, array $actual): void
    {
        foreach($expected as $key => $value) {
            if(is_array($value) && is_array($actual[$key])) {
                $this->assertCompareArrayModels($value, $actual[$key]);
            } elseif(!is_array($value) && !is_array($actual[$key])) {
                $this->assertEquals((string)$value, (string)$actual[$key]);
            } elseif((is_null($value) || $value == '') && $actual[$key] == []) {
                $this->assertTrue(true);
            } else {
                $this->assertEquals($expected, $actual, $key);
            }
        }
    }

    abstract public static function assertArrayHasKey(int|string $key, ArrayAccess|array $array, string $message = ''): void;
    abstract public static function assertFileExists(string $filename, string $message = ''): void;
}
