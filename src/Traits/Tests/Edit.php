<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use Illuminate\Http\Response;
use Cetria\Laravel\Api\Models\Params;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;
use Illuminate\Database\Eloquent\Model;

trait Edit
{
    use Arranges;
    use Helpers;

    protected function editOk(Params $params): TestResponse
    {
        $model = $this->arrangeVissible($params);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertCorrectId($response, $model);
        return $response;
    }

    protected function editUnauthorized(Params $params): TestResponse
    {
        $this->arrangeVissible($params);
        AuthHelper::logout();
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        return $response;
    }

    protected function editNotAllowed(Params $params): TestResponse
    {
        $model = $this->arrangeVissible($params);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
        return $response;
    }

    protected function act(Params $params): TestResponse
    {
        Auth::logout();
        $response = $this->json('GET', $this->getRoute('edit', $params->getRouteParams()), [], $this->getHeader());
        return $response;
    }

    protected function assertCorrectId(TestResponse $response, Model $model): void
    {
        $jsonResponse = $response->getContent();
        $message = json_decode($jsonResponse, true)['message'];
        $actionUrl = $message['params']['action'];
        $url = explode('/', $actionUrl);
        $this->assertEquals($model->getKey(), last($url));
    }

    public function getParamsForCorrectGenerate(): Params
    {
        return (new Params())
            ->setVissibleCount(1);
    }

    protected function loginConditional(): bool
    {
        return true
                && !Auth::check();
    }
}
