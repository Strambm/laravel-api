<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Casts\ArrayObject;
use Cetria\Laravel\Api\Traits\Tests\HasAuthenticatedUser;

trait Helpers
{
    protected function getParrentModelClass(): string
    {
        $controllerClass = $this->getControllerClass();
        return $controllerClass::getModelClass();

    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Helpers\LoginTest
     */
    protected function login(Authenticatable $user): void
    {
        Auth::login($user);
        if(method_exists($user, 'createToken')) {
            AuthHelper::setToken($user->createToken($this->getAuthTokenName())->plainTextToken);
        }
    }

    protected function getAuthTokenName(): string
    {
        return 'auth';
    }

    protected function logout(): void
    {
        AuthHelper::logout();
        Auth::logout();
    }

    protected function getHeader(): array
    {
        $header = [];
        if(AuthHelper::hasToken()) {
            $header['Authorization'] = AuthHelper::getToken();
        }
        return $header;
    }

    protected abstract function getBasicRouteName(): string;
    
    protected function getRoute(string $postfix, array $params = []): string
    {
        return route($this->getBasicRouteName() . '.' . $postfix, $params);
    }

    /**
     * @return TestResponse
     */
    public abstract function json($method, $uri, array $data = [], array $headers = []);

    //protected abstract function getEmptyUser(): Authenticatable;

    abstract public static function assertEquals($expected, $actual, string $message = ''): void;

    abstract public static function assertNotNull($actual, string $message = ''): void;

    protected function assertResponseMessage(TestResponse $response, Model|Collection $models): void
    {
        $responseMessage = json_decode($response->getContent(), true);
        if(array_key_exists('message', $responseMessage)) {
            $responseMessage = $responseMessage['message'];
        } 
        
        if($models instanceof Model) {
            $this->assertResponseMessageForModel($responseMessage, $models);
        } else {
            $this->assertEquals($models->count(), count($responseMessage));
            $keyName = $models->first()->getKeyName();
            foreach($responseMessage as $responseModel) {
                $model = $models->where($keyName, $responseModel[$keyName])->first();
                $this->assertResponseMessageForModel($responseModel, $model);
            }
        }
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Helpers\AssertResponseMessageForModelTest
     */
    protected function assertResponseMessageForModel(array $responseModel, Model $model): void
    {
        $attributes = array_keys($model->toArray());

        $this->assertArrayHasSameValues($attributes, array_keys($responseModel));
        foreach($attributes as $attribute) {
            if(
                $model->$attribute instanceof Collection 
                || $model->$attribute instanceof ArrayObject 
            ) {
                $this->assertEquals(json_decode(json_encode($model->$attribute->toArray()), true), $responseModel[$attribute]);
            } elseif(is_array($model->$attribute)) {
                $this->assertEquals($model->$attribute, $responseModel[$attribute]);
            } elseif(is_object($model->$attribute) && Reflection::isEnum($model->$attribute)) {
                $this->assertEquals((string)$model->$attribute->value, $responseModel[$attribute], $attribute);
            } elseif(!is_null($model->$attribute)) {
                if(
                    $model->$attribute instanceof Carbon
                    && array_key_exists($attribute, $model->getCasts())
                    && (
                        Str::contains($model->getCasts()[$attribute], 'date:')
                        || Str::contains($model->getCasts()[$attribute], 'date_time:')
                    )
                ) {
                    $dateFormat = str_replace(['date:', 'date_time:'], ['', ''], $model->getCasts()[$attribute]);
                    $this->assertEquals($model->$attribute->format($dateFormat), $responseModel[$attribute], $attribute);
                } else {
                    $this->assertEquals((string)$model->$attribute, $responseModel[$attribute], $attribute);
                }
            }
        }
        
        if(is_null($model->getKey())) {
            $this->assertNotNull($responseModel[$model->getKeyName()]);
        } else {
            $this->assertEquals($model->getKey(), $responseModel[$model->getKeyName()]);
        }
    }

    protected function assertArrayHasSameValues(array $expected, array $actual, string $message = ''): void
    {
        if(in_array('pivot', $actual)) {
            $actual = array_diff($actual, ['pivot']);
        }
        sort($expected);
        sort($actual);
        $this->assertEquals($expected, $actual, $message);
    }

    abstract protected function getControllerClass(): string;

    protected function getActualRouteParamName(): string
    {
        $basicRouteName = $this->getBasicRouteName();
        $explodedNames = explode('.', $basicRouteName);
        $key = Str::singular(last($explodedNames));
        return $key;
    }

    protected function loginTestUserIfCan(): void
    {
        if($this->loginConditional()) {
            $testUser = $this->getAuthUser();
            $this->login($testUser);
        }
    }

    protected function loginConditional(): bool
    {
        return Reflection::hasUseTrait($this, HasAuthenticatedUser::class)
                && !Auth::check();
    }
}
