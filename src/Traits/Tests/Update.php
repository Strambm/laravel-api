<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use ArrayAccess;
use Cetria\Laravel\Form\Form;
use Illuminate\Http\Response;
use Cetria\Laravel\Api\Models\Params;
use Cetria\Laravel\Api\Helpers\AuthHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Hidden;

trait Update
{
    use Arranges;
    use Helpers;

    protected $params = [];

    public function updateOk(Params $param): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeAttributes($param);
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertCompareParamsWithResponse($response);
        $this->assertCompareParamsWithStoredModel($response);
        return $response;
    }

    public function updateUnauthorized(Params $param): TestResponse
    {
        $this->arrangeAttributes($param);
        AuthHelper::logout();
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        return $response;
    }

    protected function updateNotAllowed(Params $param): TestResponse
    {
        $this->loginTestUserIfCan();
        $this->arrangeAttributes($param);
        $response = $this->act($param);
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
        return $response;
    }

    protected function arrangeAttributes(Params &$params): Model
    {
        $models = $this->arrangeVissibles($params);
        $main = $models->first();
        $temp = $models->last();
        $temp->delete();
        /** @var Form $form */
        $form = $main->toForm();
        $attributes = $form->getContent();
        $attributes = array_filter($attributes, function(Input $attribute) use ($temp): bool {
            $name = $attribute->getName();
            $attributes = $temp->getAttributes();
            return $attribute instanceof Hidden === false
                && $attributes[$name] != $attribute->getValue();
        });
        $randIndex = array_rand($attributes);
        $attribute = $attributes[$randIndex];
        /** @var Input $value */
        foreach($form->getContent() as $value) {
            $this->params[$value->getName()] = Reflection::getHiddenProperty($value, 'value');
            if($value->getName() == $randIndex) {
                $this->params[$value->getName()] = Reflection::getHiddenProperty($attribute, 'value');
            }
        }
        return $main;
    }

    private function getEmptyModelInstance(): Model
    {
        $modelClass = $this->getParrentModelClass();
        return new $modelClass();
    }

    protected function act(Params $params): TestResponse
    {
        Auth::logout();
        $paramsRAW = $this->params;
        $paramsRAW['_method'] = 'PUT';
        $response = $this->json('POST', $this->getRoute('update', $params->getRouteParams()), $paramsRAW, $this->getHeader());
        return $response;
    }

    protected function assertCompareParamsWithResponse(TestResponse $response): void
    {
        $jsonContent = $response->getContent();
        $returnedModel = json_decode($jsonContent, true)['message'];
        $this->assertArrayHasKey($this->getEmptyModelInstance()->getKeyName(), $returnedModel);
        $this->assertNotEmpty($returnedModel[$this->getEmptyModelInstance()->getKeyName()]);
        foreach($this->params as $name => $value) {
            if(array_key_exists($name, $returnedModel)) {
                $this->assertEquals($value, $returnedModel[$name], $name);
            }
        }
    }

    protected function assertCompareParamsWithStoredModel(TestResponse $response): void
    {
        $jsonContent = $response->getContent();
        $returnedModel = json_decode($jsonContent, true)['message'];
        $modelClass = $this->getParrentModelClass();
        $storedModelAttributes = $modelClass::withoutGlobalScopes()
            ->findOrFail($returnedModel[$this->getEmptyModelInstance()->getKeyName()])
            ->getAttributes();
        foreach($this->params as $name => $value) {
            $this->assertArrayHasKey($name, $storedModelAttributes);
            if(is_array($value)) {
                $value = json_decode(json_encode($value), true);
                $this->assertEquals($value, json_decode($storedModelAttributes[$name], true));
            } else {
                $this->assertEquals($value, $storedModelAttributes[$name]);
            }
            
        }
    }

    public function getParamsForCorrectGenerate(): Params
    {
        return (new Params())
            ->setVissibleCount(2);
    }

    protected function loginConditional(): bool
    {
        return !Auth::check();
    }

    abstract public static function assertArrayHasKey(string|int $key, ArrayAccess|array $array, string $message = ''): void;
    abstract public static function assertEquals($expected, $actual, string $message = ''): void;
    abstract public static function assertNotEmpty($actual, string $message = ''): void;
}
