<?php

namespace Cetria\Laravel\Api\Traits\Tests;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Cetria\Laravel\Form\Method;
use Cetria\Laravel\Filter\Filter;
use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\TestResponse;
use Cetria\Laravel\Api\Models\Params;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Api\Traits\Tests\HasUserIndependentModel;

trait Index
{
    use Arranges;
    use Helpers;

    protected function indexOk(Params $params): TestResponse
    {
        $this->cleanItemsBefeoreTesting();
        $models = $this->arrangeIndexData($params);
        $models = $this->filterExpectedResult($params, $models);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertCorrectIndexHeader($response, $params);
        $this->assertResponseMessage($response, $models);
        return $response;
    }

    protected function indexOkWithFilter(Params $params): TestResponse
    {
        $this->cleanItemsBefeoreTesting();
        $models = $this->arrangeIndexData($params);
        $params = $this->arrangeFilter($params, $models);
        $models = $this->filterExpectedResult($params, $models);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertCorrectIndexHeader($response, $params);
        $this->assertResponseMessage($response, $models);
        return $response;
    }

    protected function indexNotAllowed(Params $params): TestResponse
    {
        $this->cleanItemsBefeoreTesting();
        $models = $this->arrangeIndexData($params);
        $models = $this->filterExpectedResult($params, $models);
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
        return $response;
    }

    public function indexUnauthorized(Params $params): TestResponse
    {
        $this->cleanItemsBefeoreTesting();
        $models = $this->arrangeIndexData($params);
        $models = $this->filterExpectedResult($params, $models);
        $this->logout();
        $response = $this->act($params);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        return $response;
    }

    protected function cleanItemsBefeoreTesting(): void
    {
        $parentModelClass = $this->getParrentModelClass();
        if(!Reflection::hasUseTrait($this, HasUserIndependentModel::class)) {
            return;
        }

        /** @var Builder $queryBuilder */
        $queryBuilder = $parentModelClass::query();
        $queryBuilder->delete();
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Index\ArrangeFilterTest
     */
    protected function arrangeFilter(Params $params, Collection $models): Params
    {
        if($models->count() > 1) {
            $firstModelAttributes = $models->first()
                ->getAttributes();
            $secondModelAttributes = $models->skip(1)->first()
                ->getAttributes();
            $diff = array_diff_assoc($firstModelAttributes, $secondModelAttributes);
            if(count($diff) == 0) {
                throw new Exception('The test models are identical.');
            }
            $diffKey = array_key_first($diff);
            $filter = new Filter();
            $filter->addScope(new Condition($diffKey, Operator::EQ, $diff[$diffKey]));
            $params->addRouteParam('filter', (string) $filter);
            $filter->apply($models);
            if($params->getVissibleCount() > $models->count()) {
                $params->setVissibleCount($models->count());
            }
            return $params;
        }
        throw new Exception('For testing the filter, you need a test dataset with at least 2 instances.');
    }

    protected function filterExpectedResult(Params $params, Collection $models): Collection
    {
        $controllerClass = $this->getControllerClass();
        $controllerInstance = new $controllerClass();
        $offsetMethod = Reflection::getHiddenMethod($controllerClass, 'getOffset');
        $limitMethod = Reflection::getHiddenMethod($controllerClass, 'getLimit');
        $models = $this->filterModelsIfFilterExists($params, $models);
        $request = new Request();
        $request->merge($params->getRouteParams());
        return $models
            ->skip($offsetMethod->invoke($controllerInstance, $request))
            ->take($limitMethod->invoke($controllerInstance, $request));
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\Tests\Index\FilterModelsIfFilterExistsTest
     */
    protected function filterModelsIfFilterExists(Params $params, Collection $models): Collection
    {
        $routeParams = $params->getRouteParams();
        if(array_key_exists('filter', $routeParams)) {
            $filter = new Filter($routeParams['filter']);
            $countBefore = $models->count();
            $filter->apply($models);
            $countAfter = $models->count();
            if($countBefore == $countAfter) {
                throw new Exception('The test data remains unchanged after filtering. FilterQuery: \'' . $routeParams['filter'] . '\'');
            }
        }
        return $models;
    }

    protected function arrangeIndexData(Params $params): Collection
    {
        if(!Reflection::hasUseTrait($this, HasUserIndependentModel::class)) {
            $this->arrangeNoise($params);
        }
        $visibleModels = $this->arrangeVissibles($params);
        $key = $visibleModels->first()->getKeyName();
        $visibleModels = $visibleModels->sortBy($key);

        return $visibleModels;
    }

    protected function hasGeneratedModelKeyInUrl(int $deep = 0): bool
    {
        return $deep != 0;
    }

    protected function act(Params $params): TestResponse
    {
        Auth::logout();
        $response = $this->json(Method::GET->name, $this->getRoute('index', $params->getRouteParams()), [], $this->getHeader());
        return $response;
    }

    protected function assertCorrectIndexHeader(TestResponse $response, Params $params): void
    {
        $responseHeader = json_decode($response->getContent(), true)['header'];
        $this->assertEquals($params->getVissibleCount(), $responseHeader['itemCount']);
        $this->assertEquals((int) $params->getOffset(), $responseHeader['offset']);
    }

    public function getParamsForCorrectGenerate(): Params
    {
        return new Params();
    }
}
