<?php

namespace Cetria\Laravel\Api\Traits;

use Throwable;
use Illuminate\Http\Request;
use Cetria\Laravel\Form\Form;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Hidden;
use Cetria\Laravel\Api\Interfaces\CrudControllerInterface;
use Cetria\Laravel\Form\Interfaces\CastToFormInterface;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait HasCrudViaPivot
{
    use HasCrud {
        getIndexDataQuery as getIndexDataQueryBasic;
        getShowData as getShowDataBasic;
        makeCreateForm as parentMakeCreateForm;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\StoreTest
     */
    public function store(Request $request, int ...$ids): Response
    {
        Model::getConnectionResolver()->connection()->beginTransaction();
        try {
            $ids[] = null;
            $this->storeVerify($request);
            $relation = $this->getRelation($ids);
            $model = $this->getModelInstance($request);
            $relation->save(
                $model,
                $this->getPivotAttributesFromRequest($request, 'pivot.'),
            );
            return new Response(
                [
                    'message' => $model,
                ], 
                Response::HTTP_CREATED
            );
        } catch(Throwable $e) {
            Model::getConnectionResolver()->connection()->rollBack();
            throw $e;
        }
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\UpdateTest
     */
    public function update(Request $request, int ...$ids): Response
    {
        $this->updateVerify($request);
        $relation = $this->getRelation($ids);
        $relation->syncWithoutDetaching([
            end($ids) => $this->getPivotAttributesFromRequest($request, 'pivot.'),
        ]);
        $model = $this->getShowData($request, $ids);
        return new Response(
            [
                'message' => $model,
            ], 
            Response::HTTP_OK
        );
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\DestroyTest
     */
    public function destroy(Request $request, int ...$ids): Response
    {
        $this->destroyVerify($request);
        $this->getShowData($request, $ids);
        $relation = $this->getRelation($ids);
        $relation->detach(end($ids));
        return new Response(null, Response::HTTP_NO_CONTENT);
    }
    
    abstract protected function getControllerClassBefore(): string;

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\GetControllerInstanceBeforeTest
     */
    protected function getControllerInstanceBefore(): CrudControllerInterface
    {
        $controllerBefore = new ($this->getControllerClassBefore());
        return $controllerBefore;
    }

    abstract protected function getRelationNameFromModelBefore(): string;

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\GetIndexDataQueryTest
     */
    protected function getIndexDataQuery(Request $request, array $ids): BelongsToMany
    {
        $relation = $this->getRelation($ids);
        $query = $this->tryApplyFilter($relation, $request);
        return $query;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\GetModelBeforeTest
     */
    protected function getModelBefore(array $ids): Model
    {
        array_pop($ids);
        $controllerBefore = $this->getControllerInstanceBefore();
        $method = Reflection::getHiddenMethod($controllerBefore, 'getShowData');
        $modelBefore = $method->invoke($controllerBefore, new Request(), $ids);
        return $modelBefore;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\GetRelationTest
     */
    protected function getRelation(array $ids): BelongsToMany
    {
        $relationName = $this->getRelationNameFromModelBefore();
        $modelBefore = $this->getModelBefore($ids);
        return $modelBefore->$relationName();
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\GetModelInstanceTest
     */
    protected function getModelInstance(Request $request): Model
    {
        $attributes = $request->all();
        $instanceOfModel = $this->getEmptyModelInstance();
        if(array_key_exists($instanceOfModel->getKeyName(), $attributes)) {
            $attributes = [$instanceOfModel->getKeyName() => $attributes[$instanceOfModel->getKeyName()]];
        } else {
            $attributes = array_filter($attributes, function($key) use($instanceOfModel): bool {
                return in_array($key, $instanceOfModel->getFillable());
            }, ARRAY_FILTER_USE_KEY);
        }
        $modelClass = $this->_getModelClass();
        /** @var Model $model */
        $model = $modelClass::firstOrNew($attributes);
        return $model;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrudViaPivot\GetPivotAttributesFromRequestTest
     */
    protected function getPivotAttributesFromRequest(Request $request, string $prefix = 'pivot.')
    {
        $result = [];
        $attributes = $request->all();
        $filtered = array_filter($attributes, function(mixed $keyName) use ($prefix) {
            $explodedKey = explode('.', $keyName);
            $explodedPrefix = array_filter(explode('.', $prefix), function(string $part): bool { return strlen($part) > 0;});
            foreach(array_keys($explodedPrefix) as $key) {
                if(!(array_key_exists($key, $explodedKey) && $explodedKey[$key] == $explodedPrefix[$key])) {
                    return false;
                }
            }
            return count($explodedKey) - 1 == count($explodedPrefix);
        }, ARRAY_FILTER_USE_KEY);
        foreach($filtered as $key => $value) {
            $result[str_replace($prefix, '', $key)] = $value;
        }
        return $result;
    }

    protected function makeCreateForm(CastToFormInterface $instance): Form
    {
        $form = $this->parentMakeCreateForm($instance);
        if($instance instanceof Model) {
            $form->addContent(new Hidden($instance->getKeyName(), $instance->getKey()));
        }
        foreach($this->getFillablePivotAttributes() as $input) {
            $form->addContent($input);
        }
        return $form;
    }

    protected function makeUpdateForm(CastToFormInterface $instance): Form
    {
        $form = new Form();
        $form->setContent($this->getFillablePivotAttributes());
        return $form;
    }

    /**
     * @return Input[]
     */
    protected function getFillablePivotAttributes(): array
    {
        return [];
    }
}
