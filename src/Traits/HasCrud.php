<?php

namespace Cetria\Laravel\Api\Traits;

use Throwable;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Cetria\Laravel\Form\Form;
use Illuminate\Http\Response;
use Cetria\Laravel\Form\Method;
use Illuminate\Validation\Rule;
use Cetria\Laravel\Filter\Filter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Cetria\Laravel\Api\Tasks\MakeExport;
use Illuminate\Database\Eloquent\Builder;
use Cetria\Laravel\Api\Tasks\SendMailParams;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Api\Tasks\MakeExportParams;
use Illuminate\Validation\ValidationException;
use Cetria\Laravel\Api\Models\ExportDrivers\CSV;
use Cetria\Laravel\Api\Models\ExportDrivers\XML;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Exceptions\HttpResponseException;
use Cetria\Laravel\Form\Interfaces\CastToFormInterface;
use Illuminate\Database\Eloquent\Relations\HasOneOrMany;
use Cetria\Laravel\Api\Interfaces\EnableSendEmailsInterface;

trait HasCrud
{
    protected $itemsOnPage = 100;

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\IndexTest
     */
    public function index(Request $request, int ...$ids): Response
    {
        $ids[] = null;
        $data = $this->getIndexData($request, $ids);
        return new Response(
            [
                'header' => [
                    'itemCount' => $this->getIndexDataQuery($request, $ids)->count(),
                    'offset' => $this->getOffset($request),
                ],
                'message' => $data,
            ], 
            Response::HTTP_OK
        );
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\IndexExportTest
     */
    public function indexExport(Request $request, int ...$ids): Response
    {
        if($this instanceof EnableSendEmailsInterface) {
            $validateOptions = [
                'urlToPing' => 'required_without:email|url',
                'email' => 'required_without:urlToPing|email:rfc',
                
            ];
        } else {
            $validateOptions = [
                'urlToPing' => 'required|string|url',
            ];
        }
        $request->validate(
            array_merge([
                    'format' => [
                        'required',
                        Rule::in(array_keys($this->getExportFormats()))
                    ]
                ], 
                $validateOptions
            )
        );

        $driver = $this->getExportFormats()[$request->format];
        if(
            $this instanceof EnableSendEmailsInterface
                && !empty($request->email)
        ) {
            $params = new MakeExportParams(static::class, $ids, $driver, returnEmailsParams: new SendMailParams($request->email, $this->getEmailClass()));
        } else {
            $params = new MakeExportParams(static::class, $ids, $driver, returnUrl: $request->urlToPing);
        }
        $job = new MakeExport(
            $params->setRequestParams($request)
        );
        dispatch($job);

        return new Response(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\CreateTest
     */
    public function create(Request $request, int ...$ids): Response
    {
        $this->createVerify($request);
        /** @var CastToFormInterface $modelInstance */
        $modelInstance = $this->getEmptyModelInstance();
        if($modelInstance instanceof CastToFormInterface) {
            $form = $this->makeCreateForm($modelInstance);
            $form->applyValidators($this->storeValidateRules());
            $params = $form->getParams();
            $params->setMethod(Method::POST);
            $params->setAction($this->getNextActionUrl($request, 'store', $ids));
            return new Response(['message' => $form], Response::HTTP_OK);
        }
        return new Response(null, Response::HTTP_NOT_IMPLEMENTED);
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\StoreTest
     * @throws HttpResponseException
     * @throws ValidationException
     */
    public function store(Request $request, int ...$ids): Response
    {
        Model::getConnectionResolver()->connection()->beginTransaction();
        try {
            $this->storeVerify($request);
            $modelInstance = $this->getEmptyModelInstance();
            $this->fillModel($modelInstance, $request, '');
            $this->fillModelRelatedModels($modelInstance, $request);
            $modelInstance->save();
            $this->createHasOneOrManyRelatedModels($modelInstance, $request);
            Model::getConnectionResolver()->connection()->commit();
            return new Response(
                [
                    'message' => $modelInstance,
                ], 
                Response::HTTP_CREATED
            );
        } catch(Throwable $e) {
            Model::getConnectionResolver()->connection()->rollBack();
            throw $e;
        }
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\StoreValidateTest
     * @throws HttpResponseException
     * @throws ValidationException
     */
    public function storeValidate(Request $request, int ...$ids): Response
    {
        $this->storeVerify($request);
        return new Response(
            [
                'message' => 'OK'
            ], 
            Response::HTTP_OK
        );
    }

    /**
     * @throws HttpResponseException
     * @throws ValidationException
     */
    protected function storeVerify(Request $request): void
    {
        $this->AuthVerify();
        $request->validate($this->storeValidateRules());
    }

    protected function storeValidateRules(): array
    {
        return [];
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\ShowTest
     */
    public function show(Request $request, int ...$ids): Response
    {
        $model = $this->getShowData($request, $ids);
        return new Response($model, Response::HTTP_OK);
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\EditTest
     */
    public function edit(Request $request, int ...$ids): Response
    {
        $this->editVerify($request);
        $modelInstance = $this->getShowData($request, $ids);
        if($modelInstance instanceof CastToFormInterface) {
            $form = $this->makeUpdateForm($modelInstance);
            $form->applyValidators($this->updateValidateRules());
            $params = $form->getParams();
            $params->setMethod(Method::PUT);
            $params->setAction($this->getNextActionUrl($request, 'update', $ids));
            return new Response(['message' => $form], Response::HTTP_OK);
        }
        return new Response(null, Response::HTTP_NOT_IMPLEMENTED);
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\UpdateTest
     * @throws HttpResponseException
     */
    public function update(Request $request, int ...$ids): Response
    {
        $this->updateVerify($request);
        $model = $this->getShowData($request, $ids);
        $this->fillModel($model, $request, '');
        $model->save();
        return new Response(
            [
                'message' => $model,
            ], 
            Response::HTTP_OK
        );
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\StoreValidateTest
     * @throws HttpResponseException
     */
    public function updateValidate(Request $request, int ...$ids): Response
    {
        $this->updateVerify($request);
        return new Response(
            [
                'message' => 'OK',
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @throws HttpResponseException
     */
    protected function updateVerify(Request $request): void
    {
        $this->AuthVerify();
        $request->validate($this->updateValidateRules());
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\UpdateValidateRulesTest
     */
    protected function updateValidateRules(): array
    {
        $rules = $this->storeValidateRules();
        foreach(\array_keys($rules) as $key) {
            $rulesForKey = $rules[$key];
            if(is_string($rulesForKey)) {
                $rulesForKey = explode('|', $rulesForKey);
            }
            $rules[$key] = $rulesForKey;
            if(\in_array('required', $rulesForKey)) {
                unset($rules[$key][\array_search('required', $rules[$key])]);
                $rules[$key] = \array_values($rules[$key]);
            }
            if(count($rules[$key]) == 0) {
                unset($rules[$key]);
            }
        }
        return $rules;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\DestroyTest
     */
    public function destroy(Request $request, int ...$ids): Response
    {
        $this->destroyVerify($request);
        $model = $this->getShowData($request, $ids);
        $model->delete();
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    protected function getIndexData(Request $request, array $ids): Collection
    {
        $query = $this->getIndexDataQuery($request, $ids)
            ->offset($this->getOffset($request))
            ->take($this->getLimit($request));
        $data = $query->get();
        return $data;
    }

    protected function getShowData(Request $request, array $ids): Model
    {
        $query = $this->getIndexDataQuery($request, $ids);
//            ->where('id', last($ids));
        $model = $query->findOrFail(last($ids));
        return $model;
    }

    protected function getIndexDataQuery(Request $request, array $ids): Builder
    {
        $model = $this->_getModelClass();
        $query = $model::query();
        $query = $this->tryApplyFilter($query, $request);
        return $query;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\TryApplyFilterTest
     */
    protected function tryApplyFilter(Builder|Relation $query, Request $request): Builder|Relation
    {
        if(
            method_exists($query, 'applyFilter')
                && !empty($request->filter)
        ) {
            $filter = new Filter($request->filter);
            $query->applyFilter($filter);
        }
        return $query;
    }

    protected function getEmptyModelInstance(): Model
    {
        $model = $this->_getModelClass();
        $instance = new $model();
        return $instance;
    }

    protected function getOffset(Request $request): int
    {
        $offset = ($this->getCurrentPage($request) - 1) * $this->itemsOnPage;
        if($request->offset > 0) {
            $offset += $request->offset;
        }
        return $offset;
    }

    protected function getCurrentPage(Request $request): int
    {
        $page = (int) $request->page;
        if($page < 1) {
            return 1;
        }
        return $page;
    }

    protected function getLimit(Request $request): int
    {
        if(
            is_null($request->limit) 
            || $request->limit > $this->itemsOnPage 
            || $request->limit < 1
        ) {
            return $this->itemsOnPage;
        } else {
            return $request->limit;
        }
    }

    /**
     * @throws HttpResponseException
     */
    protected function destroyVerify(Request $request): void
    {
        $this->AuthVerify();
    }

    /**
     * @throws HttpResponseException
     */
    protected function createVerify(Request $request): void
    {
        $this->AuthVerify();
    }

    /**
     * @throws HttpResponseException
     */
    protected function editVerify(Request $request): void
    {
        $this->AuthVerify();
    }

    

    

    /**
     * @throws HttpResponseException
     */
    private function AuthVerify(): void
    {
        if(!Auth::check()) {
            $response = new Response(null, Response::HTTP_UNAUTHORIZED);
            throw new HttpResponseException($response);
        }
    }

    

    protected function makeCreateForm(CastToFormInterface $instance): Form
    {
        $form = $instance->toForm(false);
        return $form;
    } 

    protected function makeUpdateForm(CastToFormInterface $instance): Form
    {
        $form = $instance->toForm();
        return $form;
    }

    protected function fillModel(Model &$model, Request $request, string $prefix = ''): void
    {
        foreach($model->getFillable() as $attribute) {
            $attributeRequest = $prefix . $attribute;
            if(\array_key_exists($attributeRequest, $request->all())) {
                $model->$attribute = $request->$attributeRequest;
            }
        }
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\FillModelRelatedModelsTest
     */
    protected function fillModelRelatedModels(Model &$model, Request $request): void
    {
        foreach($model->getToFormRelatedModels() as $relationName => $relatedClass) {
            /** @var BelongsTo $relation */
            $relation = $model->$relationName();
            if($relation instanceof BelongsTo === false) {
                continue;
            }

            if($this->requestContainPrefix($request, $relationName . '.')) {
                /** @var Model $relatedModel */
                $relatedModel = new $relatedClass();
                $this->fillModel($relatedModel, $request, $relationName . '.');
                $relatedAttributes = $relatedModel->getAttributes();
                $related = $relatedClass::firstOrCreate($relatedAttributes);
                $relation->associate($related);
            } else {
                $model->load($relationName);
            }
        }
    }

    private function requestContainPrefix(Request $request, string $prefix): bool
    {
        foreach(array_keys($request->all()) as $name) {
            if(Str::startsWith($name, $prefix)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Traits\HasCrud\CreateHasOneOrManyRelatedModelsTest
     */
    protected function createHasOneOrManyRelatedModels(Model &$model, Request $request): void
    {
        foreach($model->getToFormRelatedModels() as $relationName => $relatedClass) {
            /** @var HasOneOrMany $relation */
            $relation = $model->$relationName();
            if($relation instanceof HasOneOrMany == false) {
                continue;
            }
            if($relation instanceof HasOne) {
                $this->createRelatedModel($relatedClass, $relationName, $relation, $request);
            } elseif($relation instanceof HasMany) {
                $relevatedRequestParams = array_filter(
                    $request->all(), 
                    function(string $key) use ($relationName): bool 
                    { 
                        return Str::startsWith($key, $relationName . '.');
                    }, 
                    ARRAY_FILTER_USE_KEY
                );
                $arrayKeys = $this->getRelatedParamsArrayKeys($relevatedRequestParams);
                foreach($arrayKeys as $index) {
                    $cloneRequest = $this->makeHasManyRelatedRequest($relevatedRequestParams, $index);
                    $this->createRelatedModel($relatedClass, $relationName, $relation, $cloneRequest);
                }
            }
            $model->load($relationName);
        }
    }

    /**
     * @return int[]
     */
    private function getRelatedParamsArrayKeys(array $params): array
    {
        $arrayKeys = [];
        foreach($params as $values) {
            if(is_array($values)) {
                $arrayKeys = array_values(array_unique(array_merge($arrayKeys, array_keys($values))));
            }
        }
        return $arrayKeys;
    }

    private function makeHasManyRelatedRequest(array $relevatedRequestParams, int $index): Request
    {
            $params = [];
            foreach($relevatedRequestParams as $key => $values) {
                if(array_key_exists($index, $values)) {
                    $params[$key] = $values[$index];
                }
            }
            $request = new Request($params);
            return $request;
    }

    private function createRelatedModel(string $relatedClass, string $relationName, HasOneOrMany $relation, Request $request): Model
    {
        /** @var Model $relatedModel */
        $relatedModel = new $relatedClass();
        $this->fillModel($relatedModel, $request, $relationName . '.');
        $result = $relation->create($relatedModel->getAttributes());
        return $result;
    }

   
    abstract public static function getModelClass(): string;

    protected function _getModelClass(): string
    {
        return static::getModelClass();
    }

    protected function getNextActionUrl(Request $request, string $nextPostfix, mixed ...$ids): string
    {
        return route($this->getRouteName($request, '.' . $nextPostfix), ...$ids);
    }

    protected function getRouteName(Request $request, string $expectedPostfix): string
    {
        $name = $request->route()->getName();
        $currentPostfix = Str::afterLast($name, '.');
        $mutetedName = Str::replace('.' . $currentPostfix, $expectedPostfix, $name);
        return $mutetedName;
    }

    protected function getExportFormats(): array
    {
        return [
            'xml' => XML::class,
            'csv' => CSV::class
        ];
    }
}
