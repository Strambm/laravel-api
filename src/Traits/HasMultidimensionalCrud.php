<?php

namespace Cetria\Laravel\Api\Traits;

use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Http\Request;
use Closure;
use Illuminate\Database\Eloquent\Builder;


trait HasMultidimensionalCrud
{
    use HasCrud {
        getIndexDataQuery as getIndexDataQueryBasic;
    }

    /**
     * For testing Create use ParentDestroyTestClass
     */
    abstract static protected function getControllerClassBefore(): string;
    abstract protected function getConnectionClosure(array $ids): Closure;

    protected function getIndexDataQuery(Request $request, array $ids): Builder
    {
        $query = $this->getIndexDataQueryBasic($request, $ids);
        $this->applyConnectionClosure($query, $ids);
        return $query;
    }

    public function applyConnectionClosure(Builder $query, array $ids)
    {
        array_pop($ids);
        $scope = static::getConnectionClosure($ids);
        $query->where($scope($query));
    }

    protected function expandIndexDataQueryWithPreviousController(Builder &$query, array $ids)
    {
        $controllerBefore = new (static::getControllerClassBefore());
        if(in_array(self::class, Reflection::classUsesRecursive($controllerBefore))) {
            $controllerBefore->applyConnectionClosure($query, $ids);
        }
    }
}
