<?php

namespace Cetria\Laravel\Api\Tasks;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class MakeExport
implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue; 
    use Queueable;

    protected $params;

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExport\ConstructorTest
     */
    public function __construct(MakeExportParams $params)
    {
        $this->params = $params;
    }

    public function handle(): void
    {
        $this->params->auth();
        $filename = $this->params->getDriver()->makeFile($this->getQueryBuilder(), $this->params);
        $this->contactUser($filename);
    }


    private function getQueryBuilder(): Builder|BelongsToMany
    {
        $controller = $this->params->getController();
        $queryMethod = Reflection::getHiddenMethod($controller, 'getIndexDataQuery');
        $query= $queryMethod->invoke($controller, $this->params->getRequest(), array_merge($this->params->getIds(), [null]));
        return $query;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExport\ContactUserTest
     */
    protected function contactUser(string $xmlFilename): void
    {
        if(!is_null($this->params->getReturnUrl())) {
            Http::post(
                $this->params->getReturnUrl(), 
                [
                    'exportUrl' => $this->params->getExportUrl($xmlFilename)
                ]
            );
        }
        if(!is_null($this->params->getReturnEmailParams())) {
            $emailAddress = $this->params->getReturnEmailParams()->getEmail();
            $emailClass = $this->params->getReturnEmailParams()->getEmailClass();
            Mail::to($emailAddress)
                ->send(new $emailClass([
                    'attachmentPath' => $xmlFilename,
                ]));
        }
    }
}
