<?php

namespace Cetria\Laravel\Api\Tasks;

class SendMailParams
{
    protected $email;
    protected $emailClass;

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\SendMailParams\ConstructorTest
     */
    public function __construct(string $email, string $mailClass)
    {
        $this->email = $email;
        $this->emailClass = $mailClass;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\SendMailParams\GetEmailTest
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\SendMailParams\GetEmailClassTest
     */
    public function getEmailClass(): string
    {
        return $this->emailClass;
    }
}
