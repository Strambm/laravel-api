<?php

namespace Cetria\Laravel\Api\Tasks;

use Cetria\Laravel\Api\Interfaces\ExportInterface;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Cetria\Helpers\Reflection\Reflection;

class MakeExportParams
{
    protected $controller;
    protected $ids;
    protected $controllerInstance = null;
    protected $returnUrl = null;
    protected $returnEmailParams = null;
    protected $user_key = null;
    protected $user_class = null;
    protected $requestParams = [];
    protected $driver = null;

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams\ConstructorTest
     */
    public function __construct(
        string $controller, 
        array $ids, 
        string $driver,
        string|null $returnUrl = null, 
        SendMailParams|null $returnEmailsParams = null
    ) {
        $this->controller = $controller;
        $this->ids = $ids;
        $this->driver = $driver;
        $this->returnUrl = $returnUrl;
        $this->returnEmailParams = $returnEmailsParams;
        $this->cacheAuth();
    }

    private function cacheAuth(): void
    {
        if(Auth::check()) {
            /** @var Model $user */
            $user = Auth::user();
            $this->user_class = get_class($user);
            $this->user_key = $user->getKey();
        }
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams\AuthTest
     */
    public function auth(): void
    {
        if($this->isLoginable()) {
            $class = $this->user_class;
            $user = $class::withoutGLobalScopes()->findOrFail($this->user_key);
            Auth::setUser($user);
        }
    }

    private function isLoginable(): bool
    {
        return !is_null($this->user_class)
                && !is_null($this->user_key);
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams\GetControllerTest
     */
    public function getController()
    {
        if(is_null($this->controllerInstance)) {
            $controllerClass = $this->controller;
            $this->controllerInstance = new $controllerClass();
        }
        return $this->controllerInstance;
    }

    public function getExportUrl(string $filename): string
    {
        $routeName = config('cetriaHasCrud.routeName');
        if($routeName) {
            return route($routeName, ['export' => $filename]);
        } else {
            return config('cetriaHasCrud.feedUrlPrefix') . '/' . $filename;
        }
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams\GetItemsOnPageTest
     */
    public function getItemsOnPage(): int
    {
        return Reflection::getHiddenProperty($this->getController(), 'itemsOnPage');
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams\GetIdsTest
     */
    public function getIds(): array
    {
        return $this->ids;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams\GetRequestTest
     */
    public function getRequest(): Request
    {
        return new Request($this->requestParams);
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams\GetReturnUrlTest
     */
    public function getReturnUrl(): string|null
    {
        return $this->returnUrl;
    }

    public function getReturnEmailParams(): SendMailParams|null
    {
        return $this->returnEmailParams;
    }

    public function getDriver(): ExportInterface
    {
        $class = $this->driver;
        return new $class();
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Tasks\MakeExportParams\SetRequestParamsTest
     */
    public function setRequestParams(array|Request $params): static
    {
        if($params instanceof Request) {
            $params = $params->all();
        }
        $this->requestParams = $params;
        return $this;
    }
}
