<?php

namespace Cetria\Laravel\Api\Models;
use Cetria\Laravel\Api\Traits\HasStates;

class Params
{
    use HasStates;

    protected $arrangeNoiseCount = 1;
    protected $arrangeVissibleItemsCount = 3;
    protected $offset = null;
    protected $limit = null;
    protected $expectedCount = null;
    protected $routeParams = [];
    protected $hasFakeParentKeys = false;

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\AddRouteParamTest
     */
    public function addRouteParam(string $key, string|int $value): static
    {
        $this->routeParams[$key] = $value;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\EnableFakeParentKeysTest
     */
    public function enableFakeParentKeys(bool $hasFakeParentKey): static
    {
        $this->hasFakeParentKeys = $hasFakeParentKey;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\GetLimitTest
     */
    public function getLimit(): int|null
    {
        return $this->limit;
    } 

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\GetNoiseCountTest
     */
    public function getNoiseCount(): int
    {
        return $this->arrangeNoiseCount;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\GetOffsetTest
     */
    public function getOffset(): int|null
    {
        return $this->offset;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\GetRouteParamsTest
     */
    public function getRouteParams(): array
    {
        $params = $this->routeParams;
        if(!is_null($this->getOffset())) {
            $params['offset'] = $this->getOffset();
        }
        if(!is_null($this->getLimit())) {
            $params['limit'] = $this->getLimit();
        }
        return $params;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\GetVissibleCountTest
     */
    public function getVissibleCount(): int
    {
        if(!is_null($this->expectedCount)) {
            return $this->expectedCount;
        }
        return $this->arrangeVissibleItemsCount;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\HasFakeParentKeysTest
     */
    public function hasFakeParentKeys(): bool
    {
        return $this->hasFakeParentKeys;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\MergeRouteParamsTest
     */
    public function mergeRouteParams(array $routeParams): static
    {
        $this->routeParams = array_merge($this->routeParams, $routeParams);
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\SetExpectedCountTest
     */
    public function setExpectedCount(int $count): static
    {
        $this->expectedCount = $count;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\SetLimitTest
     */
    public function setLimit(int|null $count): static
    {
        $this->limit = $count;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\SetOffsetTest
     */
    public function setOffset(int|null $count): static
    {
        $this->offset = $count;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\SetNoiseCountTest
     */
    public function setNoiseCount(int $count): static
    {
        if($count < 0) {
            $count = 0;
        }
        $this->arrangeNoiseCount = $count;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\Params\SetVissibleCountTest
     */
    public function setVissibleCount(int $count): static
    {
        if($count < 0) {
            $count = 0;
        }
        $this->arrangeVissibleItemsCount = $count;
        return $this;
    }
}
