<?php

namespace Cetria\Laravel\Api\Models\ExportDrivers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Cetria\Laravel\Api\Interfaces\ExportInterface;

abstract class Driver
implements ExportInterface
{
    protected function generateFilename(string $prefix = ''): string
    {
        if(Auth::check()) {
            $prefix = Auth::id() . '-' . $prefix;
        }
        do {
            $filename = $prefix . Str::random(20) . $this->getFilePostfix();
        } while(Storage::drive($this->getStorageDriverName())->exists($filename));
        return $filename; 
    }

    abstract protected function getFilePostfix(): string;

    protected function getStorageDriverName(): string
    {
        return 'indexExports';
    }
}
