<?php

namespace Cetria\Laravel\Api\Models\ExportDrivers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;
use Cetria\Laravel\Api\Tasks\MakeExportParams;
use Cetria\Laravel\Api\Interfaces\ExportInterface;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class CSV extends Driver
implements ExportInterface
{
    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\ExportDrivers\CSV\MakeFileTest
     */
    public function makeFile(Builder|BelongsToMany $query, MakeExportParams $params): string
    {
        $count = $query->count();
        $iterations = ceil($count / $params->getItemsOnPage());
        $filename = $this->generateFilename();
        $data = [];
        $columns = [];
        for($iteration = 0; $iteration < $iterations; $iteration ++) {
            $rows = $query
                ->skip($iteration * $params->getItemsOnPage())
                ->take($params->getItemsOnPage())
                ->get();
            /** @var Model $row */
            foreach($rows as $row) {
                $rowData = json_decode(json_encode($row->toArray()), true);
                $columns = array_unique(array_merge($columns, array_keys($rowData)));
                $data[] = $rowData;
            }
        }
        $output = fopen('php://temp', 'r+');
        fputcsv($output, $columns);
        foreach ($data as $row) {
            $rowData = [];
            foreach ($columns as $column) {
                $value = isset($row[$column]) ? $row[$column] : '';
                if(is_array($value)) {
                    $value = json_encode($value);
                }
                $rowData[] = $value;
            }
            fputcsv($output, $rowData);
        }
        rewind($output);
        $csvString = stream_get_contents($output);
        fclose($output);
        Storage::drive($this->getStorageDriverName())->put($filename, $csvString);
        return Storage::drive($this->getStorageDriverName())->path($filename);
    }

    protected function getFilePostfix(): string
    {
        return '.csv';
    }
}
