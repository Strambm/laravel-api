<?php

namespace Cetria\Laravel\Api\Models\ExportDrivers;

use DOMElement;
use DOMDocument;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;
use Cetria\Laravel\Api\Tasks\MakeExportParams;
use Cetria\Laravel\Api\Interfaces\ExportInterface;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class XML extends Driver
implements ExportInterface
{
    protected $xml;
    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\ExportDrivers\XML\MakeFileTest
     */
    public function makeFile(Builder|BelongsToMany $query, MakeExportParams $params): string
    {
        $count = $query->count();
        $iterations = ceil($count / $params->getItemsOnPage());
        $filename = $this->generateFilename();
        $this->xml = new DOMDocument('1.0', 'UTF-8');
        $message = $this->xml->createElement('message');
        $this->xml->appendChild($message);
        for($iteration = 0; $iteration < $iterations; $iteration ++) {
            $rows = $query
                ->skip($iteration * $params->getItemsOnPage())
                ->take($params->getItemsOnPage())
                ->get();
            /** @var Model $row */
            foreach($rows as $row) {
                $this->printToXml($message, 'item', $row);
            }
        }
        $xmlString = $this->xml->saveXML();
        Storage::drive($this->getStorageDriverName())->put($filename, $xmlString);
        return Storage::drive($this->getStorageDriverName())->path($filename);
    }

    protected function getFilePostfix(): string
    {
        return '.xml';
    }

    private function printToXml(DOMElement &$xml, string $key, mixed $data): void
    {
        if(is_object($data) && method_exists($data, 'toArray')) {
            $data = $data->toArray();
        }
        if(is_array(value: $data)) {
            $element = $this->xml->createElement($key);
            $xmlChild = $xml->appendChild($element);
            foreach($data as $childKey => $value) {
                if(is_numeric($childKey)) {
                    $childKey = 'key_' . $childKey;
                }
                $this->printToXml($xmlChild, $childKey, $value);
            }
        } else {
            $xml->appendChild(new DOMElement($key, $data));
        }
    }
}
