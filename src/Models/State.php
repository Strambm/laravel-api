<?php

namespace Cetria\Laravel\Api\Models;

use Illuminate\Database\Eloquent\Factories\Factory;

class State
{
    protected $name;
    protected $params;

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\State\ConstructorTest
     */
    public function __construct(string $name, mixed ...$params)
    {
        $this->name = $name;
        $this->params = $params;
    }

    /**
     * @see \Cetria\Laravel\Api\Tests\Unit\Models\State\ApplyTest
     */
    public function apply(Factory $factory): Factory
    {
        $stateName = $this->name;
        return $factory->$stateName(...$this->params);
    }
}
